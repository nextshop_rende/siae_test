package Tools;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class Applicazione {
    protected WebDriver driver;
    protected String finestraPrincipale = null;
    protected String finestraNorton = null;
    protected String finestraCodiceFiscale = null;
    protected String finestraDropmail = null;
    protected String finestraMyTrashMobile = null;
    protected WebDriverWait wait;
    protected WebDriverWait shortwait;
    protected Actions actions;
    protected String cellulare;
    protected static final String PROXY_USERNAME = "boial";
    protected static final String PROXY_PASSWORD = "N3iU6rCf";
    //ufficiale
//    protected static final String PATH_TO_CROME = "target/classes/chromedriver/chromedriver"; //proprio path
    //ubuntu
    protected static final String PATH_TO_CROME = "/home/mongiardo/chromedriver240"; //proprio path
    protected static final String VIRTUALHOST_CRM = "http://siaecrm.local/";
    protected static final String VIRTUALHOST_PORTALE_SIAE = "http://siaesol.local/";
    protected Logger logger;
    protected FileHandler fh;

    public static void main(String[] args){

    }
    public void initLog(String logName){
        try {
            logger = Logger.getLogger(logName);
            fh = new FileHandler(logName + ".txt");
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.addHandler(fh);
        } catch (Exception e){
            logger.info("Non è stato possibile creare il file di log");
        }
    }
    public void endLog(){
        fh.close();
    }

    protected void getBrowser(){
        System.setProperty("webdriver.chrome.driver", PATH_TO_CROME);
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10000);
        shortwait = new WebDriverWait(driver, 5);
        actions = new Actions(driver);
    }

    protected String creaCodiceFiscale(String nome, String cognome, String giornoNascita, String meseNascita, String annoNascita, String cittaNascita, String provinciaNascita, String Sesso) {
        if (finestraCodiceFiscale == null) {
            ((JavascriptExecutor) driver)
                    .executeScript("window.open('https://" + PROXY_USERNAME + ":" + PROXY_PASSWORD + "@www.codicefiscale.com/')");

            ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
            finestraCodiceFiscale = tabs.get(tabs.size() - 1);
            driver.switchTo().window(finestraCodiceFiscale);
        } else {
            driver.switchTo().window(finestraCodiceFiscale);
            WebElement ricalcola = driver.findElement(By.id("btn-recalcola"));
            actions.moveToElement(ricalcola);
            ricalcola.click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cf-cognome")));
            actions.moveToElement(driver.findElement(By.id("cf-cognome")));
        }
        driver.findElement(By.id("cf-cognome")).sendKeys(cognome);
        driver.findElement(By.id("cf-nome")).sendKeys(nome);
        driver.findElement(By.id("ddn_giorno")).sendKeys(giornoNascita);
        driver.findElement(By.id("ddn_mese")).sendKeys(meseNascita);
        driver.findElement(By.id("ddn_anno")).sendKeys(annoNascita);
        if(Sesso.equals("M")){
            driver.findElement(By.cssSelector("label[for=maschile]")).click();
        } else {
            driver.findElement(By.cssSelector("label[for=femminile]")).click();
        }
        //driver.findElement(By.cssSelector("#form > div.cf-box-bottom > div:nth-child(4) > div.cf-w-75 > div:nth-child(1) > label")).click();
        driver.findElement(By.id("comune")).sendKeys(cittaNascita);
        driver.findElement(By.id("provincia")).sendKeys(provinciaNascita);
        actions.moveToElement(driver.findElement(By.id("btn-calcola")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("btn-calcola")));
        driver.findElement(By.id("btn-calcola")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cf-calc")));
        actions.moveToElement(driver.findElement(By.id("cf-calc")));
        return driver.findElement(By.id("cf-calc")).getAttribute("value");
    }
    protected String creaCellulare(){
        if (finestraMyTrashMobile == null) {
            ((JavascriptExecutor) driver)
                    .executeScript("window.open('https://" + PROXY_USERNAME + ":" + PROXY_PASSWORD + "@www.mytrashmobile.com/')");

            ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
            finestraMyTrashMobile = tabs.get(tabs.size() - 1);
            driver.switchTo().window(finestraMyTrashMobile);
        } else {
            driver.switchTo().window(finestraMyTrashMobile);
        }
        WebElement campoNumeroGenerato = driver.findElement(By.id("select2-number-container"));
        actions.moveToElement(campoNumeroGenerato);
        campoNumeroGenerato.click();
        driver.findElement(By.cssSelector("#select2-number-results li:nth-child(3)")).click();
        String numeroGenerato = driver.findElement(By.id("select2-number-container")).getAttribute("title");
        String[] partiNumeroGenerato = numeroGenerato.split(" ");
        cellulare = partiNumeroGenerato[2];
        driver.switchTo().window(finestraPrincipale);
        return cellulare;
    }
    protected String creaTelefono() {
        return RandomStringUtils.randomNumeric(9, 10);
    }
    protected String creaPartitaIva() {
        String output = "";
        StringBuilder sb = new StringBuilder();
        for (int count = 0; count <= 6; count++) {
            int cifraCorrente = randInt(0, 9);
            sb.append(cifraCorrente);
        }
        sb.append("100");
        output = sb.toString();

        int primaCifraPari = Character.getNumericValue(output.charAt(0));
        int primaCifraDispari = Character.getNumericValue(output.charAt(1));
        int secondaCifraPari = Character.getNumericValue(output.charAt(2));
        int secondaCifraDispari = Character.getNumericValue(output.charAt(3));
        int terzaCifraPari = Character.getNumericValue(output.charAt(4));
        int terzaCifraDispari = Character.getNumericValue(output.charAt(5));
        int quartaCifraPari = Character.getNumericValue(output.charAt(6));
        int quartaCifraDispari = Character.getNumericValue(output.charAt(7));
        int quintaCifraPari = Character.getNumericValue(output.charAt(8));
        int quintaCifraDispari = Character.getNumericValue(output.charAt(9));

        int X = primaCifraPari + secondaCifraPari + terzaCifraPari + quartaCifraPari + quintaCifraDispari;

        primaCifraDispari = primaCifraDispari * 2;
        if (primaCifraDispari > 9) {
            primaCifraDispari = primaCifraDispari - 9;
        }

        secondaCifraDispari = secondaCifraDispari * 2;
        if (secondaCifraDispari > 9) {
            secondaCifraDispari = secondaCifraDispari - 9;
        }

        terzaCifraDispari = terzaCifraDispari * 2;
        if (terzaCifraDispari > 9) {
            terzaCifraDispari = terzaCifraDispari - 9;
        }

        quartaCifraDispari = quartaCifraDispari * 2;
        if (quartaCifraDispari > 9) {
            quartaCifraDispari = quartaCifraDispari - 9;
        }
        quintaCifraDispari = quintaCifraDispari * 2;
        if (quintaCifraDispari > 9) {
            quintaCifraDispari = quintaCifraDispari - 9;
        }

        int Y = primaCifraDispari + secondaCifraDispari + terzaCifraDispari + quartaCifraDispari + quintaCifraDispari;

        int T = (X + Y) % 10;

        int C = (10 - T) % 10;

        sb.append(C);

        return sb.toString();
    }
    private static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
    protected String creaEmail() {

        if(finestraDropmail == null){
            ((JavascriptExecutor) driver)
                    .executeScript("window.open('https://" + PROXY_USERNAME + ":" + PROXY_PASSWORD + "@dropmail.me/it/')");
            ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
            finestraDropmail = tabs.get(tabs.size() - 1);
            driver.switchTo().window(finestraDropmail);
        } else {
            driver.switchTo().window(finestraDropmail);
            ((JavascriptExecutor) driver).executeScript("location.reload()");
        }
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.container > div:nth-child(2) > div.well.well-sm.text-center > h2 > span.email")));
        return driver.findElement(By.cssSelector("body > div.container > div:nth-child(2) > div.well.well-sm.text-center > h2 > span.email")).getText();
    }
    protected void selectFromChosen(String name, String value) {
        String id = driver.findElement(By.name(name)).getAttribute("id");
        WebElement chosen = driver.findElement(By.id(id + "_chzn"));
        List<WebElement> results = chosen.findElements(By.cssSelector(".chzn-results li"));
        boolean found = false;
        WebElement elementToClick = null;
        for (WebElement we : results) {
            if (we.getAttribute("innerHTML").equals(value)) {
                found = true;
                elementToClick = we;
                break;
            }
        }
        if (found) {
            chosen.findElement(By.cssSelector("a")).click();
            elementToClick.click();
        }
    }
}
