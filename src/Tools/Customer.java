package Tools;

import java.util.List;

public class Customer {
    private String naturaGiuridica = "S.P.A.";
    private String tipoIscrizione = "ASSOCIATO";
    private String categoria = "EDITORE";
    private String codiceFiscale = "";
    private String partitaIva = "";
    private String email = "";
    private Boolean mandatoPostumo = false;
    private String giornoNascita = "";
    private String meseNascita = "";
    private String annoNascita = "";
    private String nome = "";
    private String cognome = "";
    private String cittaNascita = "";
    private String provinciaNascita = "";
    private String telefono = "";
    private String vatNumber = "";
    private Boolean iscrizioneVies = false;
    private Boolean iscrizioneAttivitaEconomica = false;
    private String nazioneSedeLegale = "";
    private String provinciaSedeLegale = "";
    private String comuneSedeLegale = "";
    private String comuneEsteroSedeLegale = "";
    private String dataInizioTutela = "";

    //stringhe per compilare il form di apertura ticket
    private String tipoRichiesta = "Informazione\\/Assistenza"; //cf_1897
    private String telefonoContatto = ""; //cf_1721
    private String emailContatto = ""; //cf_1753
    private String emailIstituzionale = ""; //tt_email_istituzionale
    private String tipologiaUtente = "";  // tt_tipologia_utente
    private String tipologiaAventeDiritto = ""; //tt_tipologia_avente_diritto
    private String repertorio = "MUSICA"; //tt_repertorio
    private String area = ""; //cf_1695
    private String argomento = "Arrangiamento, apporto creativo, strumentazione, notazione, trascrivibilita', elettronica, suoni indeterminati, partitura, forma, generi e campioni sonori, sperimentazione"; //cf_1697
    private String sottoArgomento = "Definizione e realizzazione progetto musicale per deposito opera"; //tt_sottoargomento
    private String altro = "Supporto a problematiche compositive/approfondimenti - Arrangiamento, apporto creativo, strumentazione, notazione, trascrivibilita', elettronica, suoni indeterminati, partitura, forma, generi e campioni sonori, sperimentazione - Definizione e realizzazione progetto musicale per deposito opera"; //ticket_title
    private String priorita = "Normale"; //ticketpriorities
    private String modalitaRichiesta = "TELEFONO"; //cf_1699
    private String posizioneSiae = ""; //tt_posizione_siae
    private String numeroContattato = "Associato\\/Mandante"; //cf_1725
    private String percorsoIVR = ""; //cf_1723
    private String portale = ""; //tt_portale
    private String descrizione = "Una descrizione qualsiasi per testare l'APERTURA di un ticket di assistenza"; //cke_editable cke_editable_themed cke_contents_ltr cke_show_borders  *class
    private String soluzione = ""; //cke_editable cke_editable_themed cke_contents_ltr cke_show_borders *class
    private String oggettoEmail = ""; //
    private String note = ""; //
    private String gruppoAssegnazione = "COPIA PRIVATA"; //tt_gruppo
    private String sottoGruppoAssegnazione = ""; //tt_sottogruppo
    private String inoltraOperatore = ""; //tt_operatore
    private String statoTicket = "Inoltrato"; //ticketstatus
    private String attualmenteAssegnatoA = ""; //assigned_user_id
    private String tipoChiusura = ""; //tt_tipo_chiusura
    private String motivoAnnullamentoRifiuto = ""; //tt_motivo_ann_rif
    private String noteMotivoSospensione = ""; //cf_2159
    private String invioPerConoscenza = ""; //cf_1775  *boolean
    private String selezionaDoc = ""; //btn add-on *class
    private String collegaDaClienteDoc = ""; //btn add-on filterDocs *class
    private String aggiungiDoc = ""; //HelpDesk_editView_fieldName_cf_2161_select   *id
    private String apriTicketRestweb = ""; //cf_2151  *boolean
    private String tipoTicketRestweb = ""; //cf_2153
    private String idTicketRestweb = ""; //HelpDesk_editView_fieldName_cf_2157  *id
    private String statoTicketRestweb = ""; //cf_2155
    private String salvaTicket = ""; //save


    public String getTelefonoContatto() {
        return telefonoContatto;
    }

    public void setTelefonoContatto(String telefonoContatto) {
        this.telefonoContatto = telefonoContatto;
    }

    public String getEmailContatto() {
        return emailContatto;
    }

    public void setEmailContatto(String emailContatto) {
        this.emailContatto = emailContatto;
    }

    public String getEmailIstituzionale() {
        return emailIstituzionale;
    }

    public void setEmailIstituzionale(String emailIstituzionale) {
        this.emailIstituzionale = emailIstituzionale;
    }

    public String getTipologiaUtente() {
        return tipologiaUtente;
    }

    public void setTipologiaUtente(String tipologiaUtente) {
        this.tipologiaUtente = tipologiaUtente;
    }

    public String getTipologiaAventeDiritto() {
        return tipologiaAventeDiritto;
    }

    public void setTipologiaAventeDiritto(String tipologiaAventeDiritto) {
        this.tipologiaAventeDiritto = tipologiaAventeDiritto;
    }

    public String getRepertorio() {
        return repertorio;
    }

    public void setRepertorio(String repertorio) {
        this.repertorio = repertorio;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArgomento() {
        return argomento;
    }

    public void setArgomento(String argomento) {
        this.argomento = argomento;
    }

    public String getSottoArgomento() {
        return sottoArgomento;
    }

    public void setSottoArgomento(String sottoArgomento) {
        this.sottoArgomento = sottoArgomento;
    }

    public String getAltro() {
        return altro;
    }

    public void setAltro(String altro) {
        this.altro = altro;
    }

    public String getPriorita() {
        return priorita;
    }

    public void setPriorita(String priorita) {
        this.priorita = priorita;
    }

    public String getModalitaRichiesta() {
        return modalitaRichiesta;
    }

    public void setModalitaRichiesta(String modalitaRichiesta) {
        this.modalitaRichiesta = modalitaRichiesta;
    }

    public String getPosizioneSiae() {
        return posizioneSiae;
    }

    public void setPosizioneSiae(String posizioneSiae) {
        this.posizioneSiae = posizioneSiae;
    }

    public String getNumeroContattato() {
        return numeroContattato;
    }

    public void setNumeroContattato(String numeroContattato) {
        this.numeroContattato = numeroContattato;
    }

    public String getPercorsoIVR() {
        return percorsoIVR;
    }

    public void setPercorsoIVR(String percorsoIVR) {
        this.percorsoIVR = percorsoIVR;
    }

    public String getPortale() {
        return portale;
    }

    public void setPortale(String portale) {
        this.portale = portale;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getSoluzione() {
        return soluzione;
    }

    public void setSoluzione(String soluzione) {
        this.soluzione = soluzione;
    }

    public String getOggettoEmail() {
        return oggettoEmail;
    }

    public void setOggettoEmail(String oggettoEmail) {
        this.oggettoEmail = oggettoEmail;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getGruppoAssegnazione() {
        return gruppoAssegnazione;
    }

    public void setGruppoAssegnazione(String gruppoAssegnazione) {
        this.gruppoAssegnazione = gruppoAssegnazione;
    }

    public String getSottoGruppoAssegnazione() {
        return sottoGruppoAssegnazione;
    }

    public void setSottoGruppoAssegnazione(String sottoGruppoAssegnazione) {
        this.sottoGruppoAssegnazione = sottoGruppoAssegnazione;
    }

    public String getInoltraOperatore() {
        return inoltraOperatore;
    }

    public void setInoltraOperatore(String inoltraOperatore) {
        this.inoltraOperatore = inoltraOperatore;
    }

    public String getStatoTicket() {
        return statoTicket;
    }

    public void setStatoTicket(String statoTicket) {
        this.statoTicket = statoTicket;
    }

    public String getAttualmenteAssegnatoA() {
        return attualmenteAssegnatoA;
    }

    public void setAttualmenteAssegnatoA(String attualmenteAssegnatoA) {
        this.attualmenteAssegnatoA = attualmenteAssegnatoA;
    }

    public String getTipoChiusura() {
        return tipoChiusura;
    }

    public void setTipoChiusura(String tipoChiusura) {
        this.tipoChiusura = tipoChiusura;
    }

    public String getMotivoAnnullamentoRifiuto() {
        return motivoAnnullamentoRifiuto;
    }

    public void setMotivoAnnullamentoRifiuto(String motivoAnnullamentoRifiuto) {
        this.motivoAnnullamentoRifiuto = motivoAnnullamentoRifiuto;
    }

    public String getNoteMotivoSospensione() {
        return noteMotivoSospensione;
    }

    public void setNoteMotivoSospensione(String noteMotivoSospensione) {
        this.noteMotivoSospensione = noteMotivoSospensione;
    }

    public String getInvioPerConoscenza() {
        return invioPerConoscenza;
    }

    public void setInvioPerConoscenza(String invioPerConoscenza) {
        this.invioPerConoscenza = invioPerConoscenza;
    }

    public String getSelezionaDoc() {
        return selezionaDoc;
    }

    public void setSelezionaDoc(String selezionaDoc) {
        this.selezionaDoc = selezionaDoc;
    }

    public String getCollegaDaClienteDoc() {
        return collegaDaClienteDoc;
    }

    public void setCollegaDaClienteDoc(String collegaDaClienteDoc) {
        this.collegaDaClienteDoc = collegaDaClienteDoc;
    }

    public String getAggiungiDoc() {
        return aggiungiDoc;
    }

    public void setAggiungiDoc(String aggiungiDoc) {
        this.aggiungiDoc = aggiungiDoc;
    }

    public String getApriTicketRestweb() {
        return apriTicketRestweb;
    }

    public void setApriTicketRestweb(String apriTicketRestweb) {
        this.apriTicketRestweb = apriTicketRestweb;
    }

    public String getTipoTicketRestweb() {
        return tipoTicketRestweb;
    }

    public void setTipoTicketRestweb(String tipoTicketRestweb) {
        this.tipoTicketRestweb = tipoTicketRestweb;
    }

    public String getIdTicketRestweb() {
        return idTicketRestweb;
    }

    public void setIdTicketRestweb(String idTicketRestweb) {
        this.idTicketRestweb = idTicketRestweb;
    }

    public String getStatoTicketRestweb() {
        return statoTicketRestweb;
    }

    public void setStatoTicketRestweb(String statoTicketRestweb) {
        this.statoTicketRestweb = statoTicketRestweb;
    }

    public String getSalvaTicket() {
        return salvaTicket;
    }

    public void setSalvaTicket(String salvaTicket) {
        this.salvaTicket = salvaTicket;
    }

 //fine variabili getter e setter per apertura ticket


    public String getToponimoSedeLegale() {

        return toponimoSedeLegale;
    }

    public void setToponimoSedeLegale(String toponimoSedeLegale) {
        this.toponimoSedeLegale = toponimoSedeLegale;
    }

    private String toponimoSedeLegale = "";

    public String getComuneEsteroSedeLegale() {
        return comuneEsteroSedeLegale;
    }

    public void setComuneEsteroSedeLegale(String comuneEsteroSedeLegale) {
        this.comuneEsteroSedeLegale = comuneEsteroSedeLegale;
    }


    public String getComuneSedeLegale() {
        return comuneSedeLegale;
    }

    public void setComuneSedeLegale(String comuneSedeLegale) {
        this.comuneSedeLegale = comuneSedeLegale;
    }

    public String getProvinciaSedeLegale() {
        return provinciaSedeLegale;
    }

    public void setProvinciaSedeLegale(String provinciaSedeLegale) {
        this.provinciaSedeLegale = provinciaSedeLegale;
    }

    public String getNazioneSedeLegale() {
        return nazioneSedeLegale;
    }

    public void setNazioneSedeLegale(String nazioneSedeLegale) {
        this.nazioneSedeLegale = nazioneSedeLegale;
    }


    public Boolean getIscrizioneAttivitaEconomica() {
        return iscrizioneAttivitaEconomica;
    }

    public void setIscrizioneAttivitaEconomica(Boolean iscrizioneAttivitaEconomica) {
        this.iscrizioneAttivitaEconomica = iscrizioneAttivitaEconomica;
    }


    public Boolean getIscrizioneVies() {
        return iscrizioneVies;
    }

    public void setIscrizioneVies(Boolean iscrizioneVies) {
        this.iscrizioneVies = iscrizioneVies;
    }


    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


    public String getProvinciaNascita() {
        return provinciaNascita;
    }

    public void setProvinciaNascita(String provinciaNascita) {
        this.provinciaNascita = provinciaNascita;
    }

    public String getProvinciaNascitaSigla() {
        return provinciaNascitaSigla;
    }

    public String getComuneNascita() {
        return comuneNascita;
    }

    public void setComuneNascita(String comuneNascita) {
        this.comuneNascita = comuneNascita;
    }

    private String comuneNascita = "";

    public void setProvinciaNascitaSigla(String provinciaNascitaSigla) {
        this.provinciaNascitaSigla = provinciaNascitaSigla;
    }

    private String provinciaNascitaSigla = "";


    public String getCittaNascita() {
        return cittaNascita;
    }

    public void setCittaNascita(String cittaNascita) {
        this.cittaNascita = cittaNascita;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAnnoNascita() {
        return annoNascita;
    }

    public void setAnnoNascita(String annoNascita) {
        this.annoNascita = annoNascita;
    }

    public String getMeseNascita() {
        return meseNascita;
    }

    public void setMeseNascita(String meseNascita) {
        this.meseNascita = meseNascita;
    }


    public String getGiornoNascita() {
        return giornoNascita;
    }

    public void setGiornoNascita(String giornoNascita) {
        this.giornoNascita = giornoNascita;
    }


    public Boolean getMandatoPostumo() {
        return mandatoPostumo;
    }

    public void setMandatoPostumo(Boolean mandatoPostumo) {
        this.mandatoPostumo = mandatoPostumo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPartitaIva() {
        return partitaIva;
    }

    public void setPartitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }


    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }


    public String getTipoIscrizione() {
        return tipoIscrizione;
    }

    public void setTipoIscrizione(String tipoIscrizione) {
        this.tipoIscrizione = tipoIscrizione;
    }

    public String getNaturaGiuridica() {
        return naturaGiuridica;
    }

    public void setNaturaGiuridica(String naturaGiuridica) {
        this.naturaGiuridica = naturaGiuridica;
    }

 // get e set del tipo di richiesta ticket

    public String getTipoRichiesta() {
        return tipoRichiesta;
    }

    public void setTipoRichiesta(String tipoRichiesta) {
        this.tipoRichiesta = tipoRichiesta;
    }

    public String getIndirizzoSedeLegale() {
        return indirizzoSedeLegale;
    }

    public void setIndirizzoSedeLegale(String indirizzoSedeLegale) {
        this.indirizzoSedeLegale = indirizzoSedeLegale;
    }

    private String indirizzoSedeLegale = "";

    public String getCivicoSedeLegale() {
        return civicoSedeLegale;
    }

    public void setCivicoSedeLegale(String civicoSedeLegale) {
        this.civicoSedeLegale = civicoSedeLegale;
    }

    private String civicoSedeLegale = "";

    public String getCapSedeLegale() {
        return capSedeLegale;
    }

    public void setCapSedeLegale(String capSedeLegale) {
        this.capSedeLegale = capSedeLegale;
    }

    private String capSedeLegale = "";

    public String getQualificaRappresentanteLegale() {
        return qualificaRappresentanteLegale;
    }

    public void setQualificaRappresentanteLegale(String qualificaRappresentanteLegale) {
        this.qualificaRappresentanteLegale = qualificaRappresentanteLegale;
    }

    private String qualificaRappresentanteLegale = "";

    public String getNomeRappresentante() {
        return nomeRappresentante;
    }

    public void setNomeRappresentante(String nomeRappresentante) {
        this.nomeRappresentante = nomeRappresentante;
    }

    private String nomeRappresentante = "";

    public String getCognomeRappresentante() {
        return cognomeRappresentante;
    }

    public void setCognomeRappresentante(String cognomeRappresentante) {
        this.cognomeRappresentante = cognomeRappresentante;
    }

    private String cognomeRappresentante = "";

    public String getGiornoNascitaRappresentante() {
        return giornoNascitaRappresentante;
    }

    public void setGiornoNascitaRappresentante(String giornoNascitaRappresentante) {
        this.giornoNascitaRappresentante = giornoNascitaRappresentante;
    }

    private String giornoNascitaRappresentante = "";

    public String getMeseNascitaRappresentante() {
        return meseNascitaRappresentante;
    }

    public void setMeseNascitaRappresentante(String meseNascitaRappresentante) {
        this.meseNascitaRappresentante = meseNascitaRappresentante;
    }

    private String meseNascitaRappresentante = "";

    public String getAnnoNascitaRappresentante() {
        return annoNascitaRappresentante;
    }

    public void setAnnoNascitaRappresentante(String annoNascitaRappresentante) {
        this.annoNascitaRappresentante = annoNascitaRappresentante;
    }

    private String annoNascitaRappresentante = "";

    public String getProvinciaNascitaSiglaRappresentante() {
        return provinciaNascitaSiglaRappresentante;
    }

    public void setProvinciaNascitaSiglaRappresentante(String provinciaNascitaSiglaRappresentante) {
        this.provinciaNascitaSiglaRappresentante = provinciaNascitaSiglaRappresentante;
    }

    private String provinciaNascitaSiglaRappresentante = "";

    public String getNazioneNascitaRappresentante() {
        return nazioneNascitaRappresentante;
    }

    public void setNazioneNascitaRappresentante(String nazioneNascitaRappresentante) {
        this.nazioneNascitaRappresentante = nazioneNascitaRappresentante;
    }

    private String nazioneNascitaRappresentante = "";

    public String getProvinciaNascitaRappresentante() {
        return provinciaNascitaRappresentante;
    }

    public void setProvinciaNascitaRappresentante(String provinciaNascitaRappresentante) {
        this.provinciaNascitaRappresentante = provinciaNascitaRappresentante;
    }

    private String provinciaNascitaRappresentante = "";

    public String getComuneNascitaRappresentante() {
        return comuneNascitaRappresentante;
    }

    public void setComuneNascitaRappresentante(String comuneNascitaRappresentante) {
        this.comuneNascitaRappresentante = comuneNascitaRappresentante;
    }

    private String comuneNascitaRappresentante = "";

    public String getSessoRappresentante() {
        return sessoRappresentante;
    }

    public void setSessoRappresentante(String sessoRappresentante) {
        this.sessoRappresentante = sessoRappresentante;
    }

    private String sessoRappresentante = "";


    public String getEmailRappresentante() {
        return emailRappresentante;
    }

    public void setEmailRappresentante(String emailRappresentante) {
        this.emailRappresentante = emailRappresentante;
    }

    private String emailRappresentante = "";


    public String getCodiceFiscaleRappresentante() {
        return codiceFiscaleRappresentante;
    }

    public void setCodiceFiscaleRappresentante(String codiceFiscaleRappresentante) {
        this.codiceFiscaleRappresentante = codiceFiscaleRappresentante;
    }

    private String codiceFiscaleRappresentante = "";

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String password = "";


    private String userName = "";

    public String getIdCRM() {
        return IdCRM;
    }

    public void setIdCRM(String idCRM) {
        IdCRM = idCRM;
    }

    private String IdCRM = "";

    public String getCellulare() {
        return cellulare;
    }

    public void setCellulare(String cellulare) {
        this.cellulare = cellulare;
    }

    private String cellulare = "";

    public static void main(String[] args) {
    }

    public String getGiornoMorte() {
        return giornoMorte;
    }

    public void setGiornoMorte(String giornoMorte) {
        this.giornoMorte = giornoMorte;
    }

    public String getMeseMorte() {
        return meseMorte;
    }

    public void setMeseMorte(String meseMorte) {
        this.meseMorte = meseMorte;
    }

    public String getAnnoMorte() {
        return annoMorte;
    }

    public void setAnnoMorte(String annoMorte) {
        this.annoMorte = annoMorte;
    }

    private String giornoMorte = "";
    private String meseMorte = "";
    private String annoMorte = "";

    public String getNazioneNascita() {
        return nazioneNascita;
    }

    public void setNazioneNascita(String nazioneNascita) {
        this.nazioneNascita = nazioneNascita;
    }

    private String nazioneNascita = "";

    public String getCittadinanza() {
        return cittadinanza;
    }

    public void setCittadinanza(String cittadinanza) {
        this.cittadinanza = cittadinanza;
    }

    private String cittadinanza = "";

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    private String sesso = "";

    public String getPrefissoInternazionale() {
        return prefissoInternazionale;
    }

    public void setPrefissoInternazionale(String prefissoInternazionale) {
        this.prefissoInternazionale = prefissoInternazionale;
    }

    private String prefissoInternazionale = "";

    public String getNumeroCellulare() {
        return numeroCellulare;
    }

    public void setNumeroCellulare(String numeroCellulare) {
        this.numeroCellulare = numeroCellulare;
    }

    private String numeroCellulare = "";

    public Boolean getForzaturaNumeroCellulare() {
        return forzaturaNumeroCellulare;
    }

    public void setForzaturaNumeroCellulare(Boolean forzaturaNumeroCellulare) {
        this.forzaturaNumeroCellulare = forzaturaNumeroCellulare;
    }

    private Boolean forzaturaNumeroCellulare = false;

    public Boolean getInvalido() {
        return invalido;
    }

    public void setInvalido(Boolean invalido) {
        this.invalido = invalido;
    }

    private Boolean invalido = false;

    public String getNazioneResidenza() {
        return nazioneResidenza;
    }

    public void setNazioneResidenza(String nazioneResidenza) {
        this.nazioneResidenza = nazioneResidenza;
    }

    private String nazioneResidenza = "";

    public String getProvinciaResidenza() {
        return provinciaResidenza;
    }

    public void setProvinciaResidenza(String provinciaResidenza) {
        this.provinciaResidenza = provinciaResidenza;
    }

    public String getComuneResidenza() {
        return comuneResidenza;
    }

    public void setComuneResidenza(String comuneResidenza) {
        this.comuneResidenza = comuneResidenza;
    }

    public String getComuneEsteroResidenza() {
        return comuneEsteroResidenza;
    }

    public void setComuneEsteroResidenza(String comuneEsteroResidenza) {
        this.comuneEsteroResidenza = comuneEsteroResidenza;
    }

    private String provinciaResidenza = "";
    private String comuneResidenza = "";
    private String comuneEsteroResidenza = "";

    public String getToponimoResidenza() {
        return toponimoResidenza;
    }

    public void setToponimoResidenza(String toponimoResidenza) {
        this.toponimoResidenza = toponimoResidenza;
    }

    public String getIndirizzoResidenza() {
        return indirizzoResidenza;
    }

    public void setIndirizzoResidenza(String indirizzoResidenza) {
        this.indirizzoResidenza = indirizzoResidenza;
    }

    public String getCivicoResidenza() {
        return civicoResidenza;
    }

    public void setCivicoResidenza(String civicoResidenza) {
        this.civicoResidenza = civicoResidenza;
    }

    public String getCapResidenza() {
        return capResidenza;
    }

    public void setCapResidenza(String capResidenza) {
        this.capResidenza = capResidenza;
    }

    private String toponimoResidenza = "";
    private String indirizzoResidenza = "";
    private String civicoResidenza = "";
    private String capResidenza = "";

    public Boolean getDomicilioDiversoDaResidenza() {
        return domicilioDiversoDaResidenza;
    }

    public void setDomicilioDiversoDaResidenza(Boolean domicilioDiversoDaResidenza) {
        this.domicilioDiversoDaResidenza = domicilioDiversoDaResidenza;
    }

    public String getNazioneDomicilio() {
        return nazioneDomicilio;
    }

    public void setNazioneDomicilio(String nazioneDomicilio) {
        this.nazioneDomicilio = nazioneDomicilio;
    }

    public String getProvinciaDomicilio() {
        return provinciaDomicilio;
    }

    public void setProvinciaDomicilio(String provinciaDomicilio) {
        this.provinciaDomicilio = provinciaDomicilio;
    }

    public String getComuneDomicilio() {
        return comuneDomicilio;
    }

    public void setComuneDomicilio(String comuneDomicilio) {
        this.comuneDomicilio = comuneDomicilio;
    }

    public String getComuneEsteroDomicilio() {
        return comuneEsteroDomicilio;
    }

    public void setComuneEsteroDomicilio(String comuneEsteroDomicilio) {
        this.comuneEsteroDomicilio = comuneEsteroDomicilio;
    }

    public String getToponimoDomicilio() {
        return toponimoDomicilio;
    }

    public void setToponimoDomicilio(String toponimoDomicilio) {
        this.toponimoDomicilio = toponimoDomicilio;
    }

    public String getIndirizzoDomicilio() {
        return indirizzoDomicilio;
    }

    public void setIndirizzoDomicilio(String indirizzoDomicilio) {
        this.indirizzoDomicilio = indirizzoDomicilio;
    }

    public String getCivicoDomicilio() {
        return civicoDomicilio;
    }

    public void setCivicoDomicilio(String civicoDomicilio) {
        this.civicoDomicilio = civicoDomicilio;
    }

    public String getCapDomicilio() {
        return capDomicilio;
    }

    public void setCapDomicilio(String capDomicilio) {
        this.capDomicilio = capDomicilio;
    }

    private Boolean domicilioDiversoDaResidenza = false;
    private String nazioneDomicilio = "";
    private String provinciaDomicilio = "";
    private String comuneDomicilio = "";
    private String comuneEsteroDomicilio = "";
    private String toponimoDomicilio = "";
    private String indirizzoDomicilio = "";
    private String civicoDomicilio = "";
    private String capDomicilio = "";

    public String getTipologiaConto() {
        return tipologiaConto;
    }

    public void setTipologiaConto(String tipologiaConto) {
        this.tipologiaConto = tipologiaConto;
    }

    private String tipologiaConto = "";

    public String getModalitaAccredito() {
        return modalitaAccredito;
    }

    public void setModalitaAccredito(String modalitaAccredito) {
        this.modalitaAccredito = modalitaAccredito;
    }

    private String modalitaAccredito = "";

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    private String iban = "";

    public String getDataInizioTutela() {
        return dataInizioTutela;
    }

    public void setDataInizioTutela(String dataInizioTutela) {
        this.dataInizioTutela = dataInizioTutela;
    }

    public String getComuneEsteroRappresentanteLegale() {
        return comuneEsteroRappresentanteLegale;
    }

    public void setComuneEsteroRappresentanteLegale(String comuneEsteroRappresentanteLegale) {
        this.comuneEsteroRappresentanteLegale = comuneEsteroRappresentanteLegale;
    }

    private String comuneEsteroRappresentanteLegale = "";

    public Boolean getUtilizzatore() {
        return utilizzatore;
    }

    public void setUtilizzatore(Boolean utilizzatore) {
        this.utilizzatore = utilizzatore;
    }

    private Boolean utilizzatore;

    public String getTiposocieta() {
        return tiposocieta;
    }

    public void setTiposocieta(String tiposocieta) {
        this.tiposocieta = tiposocieta;
    }

    private String tiposocieta = "";

    public List<Sezione> getSezioni() {
        return sezioni;
    }

    public void setSezioni(List<Sezione> sezioni) {
        this.sezioni = sezioni;
    }

    private List<Sezione> sezioni;

    public Boolean getAventeDiritto() {
        return aventeDiritto;
    }

    public void setAventeDiritto(Boolean aventeDiritto) {
        this.aventeDiritto = aventeDiritto;
    }

    private Boolean aventeDiritto = null;
}
