package Tools;
import java.util.List;

public class Sezione {
    private int idSezione;
    private String nomeSezione;
    private List<Diritto> diritti;

    public int getIdSezione() {
        return idSezione;
    }

    public void setIdSezione(int idSezione) {
        this.idSezione = idSezione;
    }

    public String getNomeSezione() {
        return nomeSezione;
    }

    public void setNomeSezione(String nomeSezione) {
        this.nomeSezione = nomeSezione;
    }

    public List<Diritto> getDiritti() {
        return diritti;
    }

    public void setDiritti(List<Diritto> diritti) {
        this.diritti = diritti;
    }

}
