package Tools;
import java.util.List;

public class Diritto {
    private int idDiritto;
    private String nomeDiritto;
    private String descrizioneDiritto;
    private List<Territorio> territori;

    public List<Territorio> getTerritori() {
        return territori;
    }

    public void setTerritori(List<Territorio> territori) {
        this.territori = territori;
    }


    public int getIdDiritto() {
        return idDiritto;
    }

    public void setIdDiritto(int idDiritto) {
        this.idDiritto = idDiritto;
    }

    public String getNomeDiritto() {
        return nomeDiritto;
    }

    public void setNomeDiritto(String nomeDiritto) {
        this.nomeDiritto = nomeDiritto;
    }

    public String getDescrizioneDiritto() {
        return descrizioneDiritto;
    }

    public void setDescrizioneDiritto(String descrizioneDiritto) {
        this.descrizioneDiritto = descrizioneDiritto;
    }

}
