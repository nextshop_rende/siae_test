package Tools;

public class Territorio {
    public int getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(int idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getNomeTerritorio() {
        return nomeTerritorio;
    }

    public void setNomeTerritorio(String nomeTerritorio) {
        this.nomeTerritorio = nomeTerritorio;
    }

    public Boolean getIncluso() {
        return incluso;
    }

    public void setIncluso(Boolean incluso) {
        this.incluso = incluso;
    }

    private int idTerritorio;
    private String nomeTerritorio;
    private Boolean incluso;
}
