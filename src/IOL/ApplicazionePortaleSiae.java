package IOL;

import Tools.Applicazione;
import Tools.Customer;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ApplicazionePortaleSiae extends Applicazione {
    private String ambiente;
    public static void main(String[] args){
    }
    private void connect(String ambiente){
        this.ambiente = ambiente;
        String url = "";
        getBrowser();
        driver.manage().window().maximize();
        switch (ambiente){
            case "locale":
                url =  VIRTUALHOST_PORTALE_SIAE + "it/autori-ed-editori";
                driver.get(url);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#popup-buttons > button")));
                driver.findElement(By.cssSelector("#popup-buttons > button")).click();
                driver.get(VIRTUALHOST_PORTALE_SIAE + "it/autori-ed-editori/iscriversi");
                finestraPrincipale = driver.getWindowHandle();
                break;
            case "BAMBOO":
                driver.get("https://prodtest:9flUu891f@admin.BAMBOO.siae.it/it/autori-ed-editori");
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#popup-buttons > button")));
                driver.findElement(By.cssSelector("#popup-buttons > button")).click();
                driver.get("https://admin.BAMBOO.siae.it/it/autori-ed-editori/iscriversi");
                finestraPrincipale = driver.getWindowHandle();
                break;
            case "produzione":
                url = "https://www.siae.it/it/autori-ed-editori";
                driver.get(url);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#popup-buttons > button")));
                driver.findElement(By.cssSelector("#popup-buttons > button")).click();
                driver.get("https://www.siae.it/it/autori-ed-editori/iscriversi");
                finestraPrincipale = driver.getWindowHandle();
                break;
            default:
                if (driver instanceof JavascriptExecutor) {
                    ((JavascriptExecutor) driver)
                            .executeScript("alert('Non è possibile proseguire perché l\\'ambiente impostato non esiste');");
                }
        }
    }
    public  void iscrivi(String ambiente, Customer customer){
        String nome;
        String cognome;
        String codiceFiscale;
        String sesso;
        String partitaIva;
        String email;
        String giornoNascita;
        String meseNascita;
        String annoNascita;
        String provinciaNascitaSigla;
        String comuneNascita;
        String cellulare;
        connect(ambiente);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[id^=edit-forward")));

        if(!customer.getUserName().equals("") || !customer.getPassword().equals("")){
            WebElement step0Username = driver.findElement(By.name("step_0[login_block][input_username]"));
            actions.moveToElement(step0Username);
            step0Username.sendKeys(customer.getUserName());

            WebElement step0Password = driver.findElement(By.name("step_0[login_block][input_password]"));
            actions.moveToElement(step0Password);
            step0Password.sendKeys(customer.getPassword());
        }

        WebElement step1Procedi = driver.findElement(By.cssSelector("[id^=edit-forward]"));
        actions.moveToElement(step1Procedi);
        step1Procedi.click();

        if(customer.getNome().equals("")){
            nome = RandomStringUtils.randomAlphabetic(12).toUpperCase();
        } else {
            nome = customer.getNome();
        }
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("step_1[input_nome]")));
        WebElement step1Nome = driver.findElement(By.name("step_1[input_nome]"));
        actions.moveToElement(step1Nome);
        step1Nome.sendKeys(nome);

        if(customer.getCognome().equals("")){
            cognome = RandomStringUtils.randomAlphabetic(12).toUpperCase();
        } else {
            cognome = customer.getCognome();
        }
        WebElement step1Cognome = driver.findElement(By.name("step_1[input_cognome]"));
        actions.moveToElement(step1Cognome);
        step1Cognome.sendKeys(cognome);

        if(customer.getGiornoNascita().equals("")){
            giornoNascita = "01";
        } else {
            giornoNascita = customer.getGiornoNascita();
        }
        if(customer.getMeseNascita().equals("")){
            meseNascita = "01";
        } else {
            meseNascita = customer.getMeseNascita();
        }
        if(customer.getAnnoNascita().equals("")){
            annoNascita = "1980";
        } else {
            annoNascita = customer.getAnnoNascita();
        }
        if(customer.getProvinciaNascitaSigla().equals("")){
            provinciaNascitaSigla = "RM";
        } else {
            provinciaNascitaSigla = customer.getProvinciaNascitaSigla();
        }
        if(customer.getComuneNascita().equals("")){
            comuneNascita = "ROMA";
        } else {
            comuneNascita = customer.getComuneNascita();
        }
        if(customer.getSessoRappresentante().equals("")){
            sesso = "M";
        } else {
            sesso = customer.getSessoRappresentante();
        }
        if(customer.getCodiceFiscale().equals("")){
            codiceFiscale = creaCodiceFiscale(nome, cognome, giornoNascita, meseNascita, annoNascita, comuneNascita, provinciaNascitaSigla, sesso);
            driver.switchTo().window(finestraPrincipale);
        } else {
            codiceFiscale = customer.getCodiceFiscale();
        }
        WebElement step1CodiceFiscale = driver.findElement(By.name("step_1[input_codice_fiscale]"));
        actions.moveToElement(step1CodiceFiscale);
        step1CodiceFiscale.sendKeys(codiceFiscale);

        cellulare = creaCellulare();
        WebElement fieldCellulare = driver.findElement(By.name("step_1[input_cellulare]"));
        actions.moveToElement(fieldCellulare);
        fieldCellulare.sendKeys(cellulare);

        if(customer.getEmail().equals("")){
            email = creaEmail();
            driver.switchTo().window(finestraPrincipale);
        } else {
            email = customer.getEmail();
        }
        WebElement campoEmail = driver.findElement(By.name("step_1[input_email]"));
        actions.moveToElement(campoEmail);
        campoEmail.sendKeys(email);

        WebElement pulsanteConferma = driver.findElement(By.id("edit-step-1-interroga-crm09"));
        actions.moveToElement(pulsanteConferma);
        pulsanteConferma.click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.pageLoader")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[id^=edit-step-1-input-cellulare-check-button]")));
        WebElement pulsanteOtpCellulare = driver.findElement(By.cssSelector("[id^=edit-step-1-input-cellulare-check-button]"));
        actions.moveToElement(pulsanteOtpCellulare);
        pulsanteOtpCellulare.click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.pageLoader")));
        if(!ambiente.equals("locale")){
            driver.switchTo().window(finestraMyTrashMobile);
            WebElement pulsanteRiceviOTP = driver.findElement(By.cssSelector("[type=submit][value=Receive]"));
            actions.moveToElement(pulsanteRiceviOTP);
            pulsanteRiceviOTP.click();
            ((JavascriptExecutor) driver).executeScript("document.getElementById('ajax_receive').scrollIntoView(true);");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#ajax_receive > div > div > table > tbody > tr > td.message-content:contains('SIAE')")));
            List<WebElement> textMessages = driver.findElements(By.cssSelector("#ajax_receive > div > div > table > tbody > tr > td.message-content:contains('SIAE')"));

            String messaggioRicevuto = textMessages.get(0).getText();
            String[] partiMessaggioRicevuto = messaggioRicevuto.split("PIN:");
            if(partiMessaggioRicevuto.length > 0){
                String otpRicevuto = partiMessaggioRicevuto[partiMessaggioRicevuto.length-1];
                driver.switchTo().window(finestraPrincipale);
                WebElement casellaOtpCellulare = driver.findElement(By.name("step_1[input_cellulare_check]"));
                actions.moveToElement(casellaOtpCellulare);
                casellaOtpCellulare.sendKeys(otpRicevuto);
            }
        } else {

        }


    }

}
