package BAMBOO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Test {
    protected WebDriverWait wait;
    protected WebDriverWait shortwait;
    protected Actions actions;
    protected ChromeOptions chromeOptions;
    protected static final String PATH_TO_CROME = "/home/siae_test/src/resource/chromedriver240"; //proprio path
    protected WebDriver driver;


//        driver = new ChromeDriver("--no-sandbox", "--headless", "--disable-gpu", "--window-size=1050,900");


    private void connect(String url){

        //impostazione
        System.setProperty("webdriver.chrome.driver", PATH_TO_CROME);
        chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("disable-web-security");
        chromeOptions.addArguments("no-sandbox");
        chromeOptions.addArguments("disable-setuid-sandbox");
        chromeOptions.addArguments("window-size=1920x1080");
        chromeOptions.addArguments("disable-dev-shm-usage");

        //set for Bamboo
        driver = new ChromeDriver(chromeOptions);
        //set for TestDeveloper
//        driver = new ChromeDriver();




        wait = new WebDriverWait(driver, 10000);
        shortwait = new WebDriverWait(driver, 5);
        actions = new Actions(driver);
        driver.manage().window().maximize();


        driver.get(url);

        //chiudi il driver
        driver.quit();
        System.out.println("ok ho aperto il sito");
        }//fine metodo connect



//    Main per prova
    public static void main(String[] args){
        Test t = new Test();
        t.connect("http://www.casa.it");

    }
}