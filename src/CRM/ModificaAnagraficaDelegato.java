package CRM;

import Tools.Customer;

public class ModificaAnagraficaDelegato {
    public static void main(String[] args){
        Customer cliente = new Customer();
        //cliente.setIdCRM("12121365"); //PRODUZIONE
        cliente.setIdCRM("14589383"); //TEST
        ApplicazioneCrm applicazione = new ApplicazioneCrm();
        applicazione.connect("BAMBOO");
        applicazione.apriAnagraficaPerFiltri(cliente);
    }
}
