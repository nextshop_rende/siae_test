package CRM;

import Tools.Applicazione;
import Tools.Customer;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ApplicazioneCrm extends Applicazione {
    public static void main(String[] args) {
    }

    public void initLog(String logName) {

        super.initLog(logName);
    }

    public void endLog() {

        super.endLog();
    }

    public void connect(String ambiente) {
        logger.info("Avvio la connessione all'ambiente " + ambiente);
        String url = "";
        try {
            this.getBrowser();
            logger.info("Connessione avvenuta con successo.");
        } catch (Exception e) {
            logger.severe("Si è verificata la seguente eccezione nel corso della connessione " + e.getMessage());
        }
        driver.manage().window().maximize();
        logger.info("Inizio le operazioni di login");
        switch (ambiente) {
            case "locale":
                url = VIRTUALHOST_CRM + "index.php";
                driver.get(url);
                driver.findElement(By.id("username")).sendKeys("admin");
                driver.findElement(By.id("password")).sendKeys("ndndslpda");
                driver.findElement(By.id("forgotPassword")).findElement(By.tagName("button")).click();
                logger.info("Login avvenuto con successo");
                break;
            case "BAMBOO":
                url = "http://" + PROXY_USERNAME + ":" + PROXY_PASSWORD + "@portaleintranet.BAMBOO.siae/?ReturnURL=CRMANG";
                driver.get(url);
                driver.findElement(By.id("ctl00_login1_username")).sendKeys("crmuser");
                driver.findElement(By.id("ctl00_login1_password")).sendKeys("pUwJJT0Fvd");
                driver.findElement(By.id("ctl00_login1_Button1")).click();
                logger.info("Login avvenuto con successo");
                break;
            case "produzione":
                url = "http://" + PROXY_USERNAME + ":" + PROXY_PASSWORD + "@menusiae/";
                driver.get(url);
                driver.findElement(By.id("ctl00_login1_username")).sendKeys("lucia");
                driver.findElement(By.id("ctl00_login1_password")).sendKeys("paolo2018");
                driver.findElement(By.id("ctl00_login1_Button1")).click();
                driver.findElement(By.id("ctl00_menu_rptAccessoDiretto_ctl00_LnkAccessoDiretto")).click();
                logger.info("Login avvenuto con successo");
                break;
            default:
                if (driver instanceof JavascriptExecutor) {
                    ((JavascriptExecutor) driver)
                            .executeScript("alert('Non è possibile proseguire perché l\\'ambiente impostato non esiste');");
                }
                logger.severe("Non è possibile proseguire perché l'ambiente impostato " + ambiente + "non esiste");
                return;
        }
        this.finestraPrincipale = driver.getWindowHandle();
    }

    public void apriElencoIscrizioniOffline() {
        driver.findElement(By.id("menubar_item_IscrizioneOffline")).click();
    }

    //metodo per effettuare una variazione di cambio rapporto
    public void variazioneCambioRapporto(){

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("Ornella");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("Vanoni");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("VANONI")));
        driver.findElement(By.linkText("VANONI")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("button.btn.dropdown-toggle")));
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Cambio rapporto")));
        driver.findElement(By.linkText("Cambio rapporto")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("10-04-2018");


        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();

        try {
            Thread.sleep(6000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_data_inizio")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_data_inizio")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_data_inizio")).sendKeys("01-01-2018");

       String tipoIscrizione = driver.findElement(By.xpath("//td[2]/div/span/div/a/span")).getText();

       if (tipoIscrizione.contentEquals("MANDANTE")) {

            tipoIscrizione = "ASSOCIATO";

       } else if (tipoIscrizione.contentEquals("ASSOCIATO")) {
            tipoIscrizione = "MANDANTE";

        }

        //System.out.println(tipoIscrizione);

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("vas_tipo_iscrizione")));
        WebElement fieldTipoIscrizione = driver.findElement(By.name("vas_tipo_iscrizione"));
        actions.moveToElement(fieldTipoIscrizione);
        selectFromChosen("vas_tipo_iscrizione", tipoIscrizione);

         try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[3]/div/button")));
        driver.findElement(By.xpath("//div[3]/div/button")).click();

        try {
            Thread.sleep(8000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if (driver.findElement(By.xpath("//div[9]/div[3]/div/button/span")).isEnabled()){
            driver.findElement(By.xpath("//div[9]/div[3]/div/button/span")).click();
        }

        if (tipoIscrizione.contentEquals("MANDANTE")) {

            tipoIscrizione = "ASSOCIATO";

        } else if (tipoIscrizione.contentEquals("ASSOCIATO")) {
            tipoIscrizione = "MANDANTE";

        }

        //System.out.println(tipoIscrizione);

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("vas_tipo_iscrizione")));
        WebElement fieldTipoIscrizionedue = driver.findElement(By.name("vas_tipo_iscrizione"));
        actions.moveToElement(fieldTipoIscrizionedue);
        selectFromChosen("vas_tipo_iscrizione", tipoIscrizione);

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[3]/div/button")));
        driver.findElement(By.xpath("//div[3]/div/button")).click();

        try {
            Thread.sleep(8000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if (driver.findElement(By.xpath("//div[9]/div[3]/div/button/span")).isEnabled()){
            driver.findElement(By.xpath("//div[9]/div[3]/div/button/span")).click();
        }

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#menubar_item_Contacts > strong")));
        driver.findElement(By.cssSelector("#menubar_item_Contacts > strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("Ornella");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("Vanoni");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("VANONI")));
        driver.findElement(By.linkText("VANONI")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Documenti")));
        driver.findElement(By.linkText("Documenti")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//th[4]/a")));
        driver.findElement(By.xpath("//th[4]/a")).click();


        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);
    }

    //metodo per effettuare una variazione di nome d'arte
    public void variazioneNomeDarte(){

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("Ornella");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("Vanoni");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("VANONI")));
        driver.findElement(By.linkText("VANONI")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("button.btn.dropdown-toggle")));
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Nome d'arte/Detto")));
        driver.findElement(By.linkText("Nome d'arte/Detto")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("10-04-2018");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_nome_d_arte")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_nome_d_arte")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_nome_d_arte")).sendKeys("NomeTest");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_detto")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_detto")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_detto")).sendKeys("DettoTest");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[3]/div/button")));
        driver.findElement(By.xpath("//div[3]/div/button")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#menubar_item_Contacts > strong")));
        driver.findElement(By.cssSelector("#menubar_item_Contacts > strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("Ornella");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("Vanoni");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("VANONI")));
        driver.findElement(By.linkText("VANONI")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[4]/tbody/tr[2]/td[2]/span")));
        String nomeDarte =  driver.findElement(By.xpath("//table[4]/tbody/tr[2]/td[2]/span")).getText();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[4]/tbody/tr[3]/td[2]/span")));
        String dettoDarte =  driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[2]/span")).getText();

        if((nomeDarte.equals("NOMETEST"))&&((dettoDarte.equals("DETTOTEST")))){

             System.out.println("La variazione NOME D'ARTE è stata eseguita con SUCCESSO");

        } else{

            System.out.println("La variazione NOME D'ARTE NON È STATA ESEGUITA SUCCESSO");
        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }

    //metodo per effettuare una variazione di pseudonimo
    public void variazionePseudonimo(){

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("Ornella");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("Vanoni");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("VANONI")));
        driver.findElement(By.linkText("VANONI")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("button.btn.dropdown-toggle")));
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Pseudonimi")));
        driver.findElement(By.linkText("Pseudonimi")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("10-04-2018");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();

        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if (driver.findElement(By.cssSelector("#ui-dialog-title-1")).isEnabled()){
            driver.findElement(By.xpath("//div[9]/div[3]/div/button[1]/span")).click();
        }

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[6]/tbody/tr/td[2]/div/span/div/a/span")));
        String qualifica =  driver.findElement(By.xpath("//table[6]/tbody/tr/td[2]/div/span/div/a/span")).getText();
        System.out.println(qualifica);

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_data_decorrenza")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_data_decorrenza")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_data_decorrenza")).sendKeys("10-04-2018");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_pseud_nome")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_pseud_nome")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_pseud_nome")).sendKeys("PseudonimoTest");

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("procedi")));
        driver.findElement(By.id("procedi")).click();

        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if (driver.findElement(By.cssSelector("#ui-dialog-title-frame_mail")).isEnabled()){
            driver.findElement(By.xpath("//div[9]/div[11]/div/button[1]")).click();
        }

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[3]/a/strong")));
        driver.findElement(By.xpath("//li[3]/a/strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("Ornella");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("Vanoni");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("VANONI")));
        driver.findElement(By.linkText("VANONI")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Qualifiche")));
        driver.findElement(By.linkText("Qualifiche")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if(driver.findElement((By.xpath("//td[3]/a"))).isEnabled()) {
        String qualificaDue = driver.findElement(By.xpath("//td[3]/a")).getText();

              if(qualificaDue.contentEquals("COREOGRAFO")){
                driver.findElement(By.xpath("//td[3]/a")).click();
                }
        }

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Pseudonimi")));
        driver.findElement(By.linkText("Pseudonimi")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[11]/td[2]/a")));
        if(driver.findElement(By.xpath("//tr[11]/td[2]/a")).isEnabled()) {

           if(driver.findElement(By.xpath("//tr[11]/td[2]/a")).getText().contentEquals("PseudonimoTest")){

               System.out.println("La variazione Pseudonimo è stata eseguita con SUCCESSO");
           } else{

               System.out.println("La variazione Pseudonimo NON È STATA ESEGUITA SUCCESSO");
           }

        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);
    }

    //metodo per effettuare una variazione di diritti & territori
    public void variazioneDirittiTerritori(){

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("Ornella");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("Vanoni");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("VANONI")));
        driver.findElement(By.linkText("VANONI")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Diritti e Territori")));
        driver.findElement(By.linkText("Diritti e Territori")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("02-11-2017");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(//button[@type='button'])[3]")));
        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();

        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#accordion-sezioni > h3:nth-child(1) > span")));
        driver.findElement(By.cssSelector("#accordion-sezioni > h3:nth-child(1) > span")).click();

       try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#accordion-diritti > h3:nth-child(1) > input")));
        driver.findElement(By.cssSelector("#accordion-diritti > h3:nth-child(1) > input")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#accordion-sezioni > h3.scelte_utente_riga_sezione.ui-accordion-header.ui-helper-reset.ui-state-default.ui-corner-all > span")));
        driver.findElement(By.cssSelector("#accordion-sezioni > h3.scelte_utente_riga_sezione.ui-accordion-header.ui-helper-reset.ui-state-default.ui-corner-all > span")).click();

       try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[2]/h3/img")));
        driver.findElement(By.xpath("//div[2]/h3/img")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[2]/div/div[2]/img")));
        driver.findElement(By.xpath("//div[2]/div/div[2]/img")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("procedi")));
        driver.findElement(By.id("procedi")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(//button[@type='button'])[3]")));
        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();


        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[3]/a/strong")));
        driver.findElement(By.xpath("//li[3]/a/strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("Ornella");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("Vanoni");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("VANONI")));
        driver.findElement(By.linkText("VANONI")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Diritti e Territori")));
        driver.findElement(By.linkText("Diritti e Territori")).click();

        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#accordion-sezioni > h3:nth-child(1) > span")));
        driver.findElement(By.cssSelector("#accordion-sezioni > h3:nth-child(1) > span")).click();

        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#accordion-sezioni > h3:nth-child(1) > span")));
        driver.findElement(By.cssSelector("#accordion-sezioni > h3:nth-child(1) > span")).click();


        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("span.ui-icon.ui-icon-triangle-1-s")));
        driver.findElement(By.cssSelector("span.ui-icon.ui-icon-triangle-1-s")).click();


        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }


    //metodo per effettuare un ripristino
    public void variazioneRipristino(){


        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.get("http://crmha.BAMBOO.siae/index.php?module=PosizioniSiae&view=Edit&record=14590165&superadmin=1");

        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("[type=checkbox][name=cancellato]")));
        WebElement cancellato = driver.findElement(By.cssSelector("[type=checkbox][name=cancellato]"));
        if (cancellato.isSelected()){} else {

            actions.moveToElement(cancellato);
            cancellato.click();

        }

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#PosizioniSiae_editView_fieldName_data_delibera_riammissione")));
        driver.findElement(By.cssSelector("#PosizioniSiae_editView_fieldName_data_delibera_riammissione")).clear();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#PosizioniSiae_editView_fieldName_data_riammissione")));
        driver.findElement(By.cssSelector("#PosizioniSiae_editView_fieldName_data_riammissione")).clear();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("button.btn.btn-success")));
        driver.findElement(By.cssSelector("button.btn.btn-success")).submit();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[3]/a/strong")));
        driver.findElement(By.xpath("//li[3]/a/strong")).click();


        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("button.btn.dropdown-toggle")));
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Ripristino")));
        driver.findElement(By.linkText("Ripristino")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("10-04-2018");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("procedi")));
        driver.findElement(By.id("procedi")).click();

       try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[3]/a/strong")));
        driver.findElement(By.xpath("//li[3]/a/strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#PosizioniSiae_detailView_fieldValue_cancellato > span.value")));
        String motivoDimissioneTotale = driver.findElement(By.cssSelector("#PosizioniSiae_detailView_fieldValue_cancellato > span.value")).getText();

        if(motivoDimissioneTotale.contentEquals("No")){

            System.out.println("La variazione di RIPRISTINO è stata fatta CORRETTAMENTE");

        } else {

            System.out.println("La variazione di RIPRISTINO NON È STATA ESEGUITA CORRETTAMENTE");
        }


        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }


    //metodo per effettuare una dimissione totale
    public void variazioneDimissioneTotale(){

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("button.btn.dropdown-toggle")));
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Cancellazione rapporto")));
        driver.findElement(By.linkText("Cancellazione rapporto")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("10-04-2018");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("procedi")));
        driver.findElement(By.id("procedi")).click();

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("body > div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-draggable > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button > span")));
        driver.findElement(By.cssSelector("body > div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-draggable > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button > span")).click();

        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[3]/a/strong")));
        driver.findElement(By.xpath("//li[3]/a/strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#PosizioniSiae_detailView_fieldValue_motivo_cancellazione > span.value")));
        String motivoDimissioneTotale = driver.findElement(By.cssSelector("#PosizioniSiae_detailView_fieldValue_motivo_cancellazione > span.value")).getText();

        if(motivoDimissioneTotale.contentEquals("DIMISSIONE")){

            System.out.println("La variazione di dimissione totale è stata fatta CORRETTAMENTE");

        } else {

            System.out.println("La variazione di dimissione totale NON È STATA ESEGUITA CORRETTAMENTE");
        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }

    //metodo per effettuare una dimissione parziale
    public void variazioneDimissioneParziale(){

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.get("http://crmha.BAMBOO.siae/index.php?module=PosizioniSiae&view=Edit&record=14590165&superadmin=1");

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("data_fine_lirica")));
        driver.findElement(By.name("data_fine_lirica")).clear();

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("button.btn.btn-success")));
        driver.findElement(By.cssSelector("button.btn.btn-success")).submit();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[3]/a/strong")));
        driver.findElement(By.xpath("//li[3]/a/strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Dimissione parziale")));
        driver.findElement(By.linkText("Dimissione parziale")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("10-04-2018");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();


        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if (driver.findElement(By.xpath("(//button[@type='button'])[3]")).isEnabled()){
            driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
        }

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_sezione_lirica")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_sezione_lirica")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("procedi")));
        driver.findElement(By.id("procedi")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(//button[@type='button'])[3]")));
        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();


        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[3]/a/strong")));
        driver.findElement(By.xpath("//li[3]/a/strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#PosizioniSiae_detailView_fieldValue_data_fine_lirica > span.value")));
        String dataDimissioneParziale = driver.findElement(By.cssSelector("#PosizioniSiae_detailView_fieldValue_data_fine_lirica > span.value")).getText();

        if(dataDimissioneParziale.contentEquals("01-01-2019")){

            System.out.println("La variazione di dimissione parziale è stata fatta CORRETTAMENTE");

        } else {

            System.out.println("La variazione di dimissione parziale NON È STATA ESEGUITA CORRETTAMENTE");
        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }

    //metodo per effettuare un estensione di tutela
    public void variazioneEstensioneTutela(){

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Estensione di tutela")));
        driver.findElement(By.linkText("Estensione di tutela")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("02-11-2017");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();


         try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if (driver.findElement(By.xpath("(//button[@type='button'])[3]")).isEnabled()){
            driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
        }


        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_sezione_cinema")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_sezione_cinema")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Modifica")));
        driver.findElement(By.linkText("Modifica")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("selettore_diritto_6")));
        driver.findElement(By.name("selettore_diritto_6")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("procedi")));
        driver.findElement(By.id("procedi")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(//button[@type='button'])[3]")));
        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();


        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("strong")));
        driver.findElement(By.cssSelector("strong")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//a/span/span")));
        driver.findElement(By.xpath("//a/span/span")).click();
        driver.findElement(By.xpath("//div[3]/ul/li[1]/ul/li[6]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//td[4]/div/input")));
        driver.findElement(By.xpath("//td[4]/div/input")).clear();
        driver.findElement(By.xpath("//td[4]/div/input")).sendKeys("DavideTest SerraTest");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[9]/button")));
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[9]/button")).click();

        try {
            Thread.sleep(4000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[2]/td[3]/a")));
        driver.findElement(By.xpath("//tr[2]/td[3]/a")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Diritti e Territori")));
        driver.findElement(By.linkText("Diritti e Territori")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@id='accordion-sezioni']/h3")));
        driver.findElement(By.xpath("//div[@id='accordion-sezioni']/h3")).click();

        String[] stati = new String[6];
        String[] statiNome = new String[6];

        for(int i=1; i<6; i++){

            try {
                Thread.sleep(4000);//1000 milliseconds is one second.
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@id='accordion-diritti']/h3["+i+"]/span[2]")));
            WebElement stato =  driver.findElement(By.xpath("//div[@id='accordion-diritti']/h3["+i+"]/span[2]"));
            stati[i] = stato.getText();

            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@id='accordion-diritti']/h3["+i+"]/span[3]")));
            WebElement statoNome =  driver.findElement(By.xpath("//div[@id='accordion-diritti']/h3["+i+"]/span[3]"));
            statiNome[i] = statoNome.getText();


          //  System.out.println("\n stato: "+i+" "+stati[i]+" Nome stato: "+statiNome[i]);


        } if((stati[1].contentEquals("non attivo") && statiNome[1].contentEquals("DIRITTO DI PUBBLICA ESECUZIONE"))
                && (stati[2].contentEquals("attivo") && statiNome[2].contentEquals("DIFFUSIONE RADIOTELEVISIVA (RADIO E TV)"))
                && (stati[3].contentEquals("attivo") && statiNome[3].contentEquals("DIRITTO DI DIFFUSIONE ONLINE"))
                && (stati[4].contentEquals("attivo") && statiNome[4].contentEquals("DIRITTO DI RIPRODUZIONE MECCANICA E DI DISTRIBUZIONE"))
                && (stati[5].contentEquals("attivo") && statiNome[5].contentEquals("DIRITTO DI NOLEGGIO E DIRITTO DI PRESTITO")
                )){

            System.out.println("La variazione di estensione di tutela è stata fatta CORRETTAMENTE");

        } else {

            System.out.println("La variazione di estensione di tutela NON È STATA ESEGUITA CORRETTAMENTE");
        }

        /*try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[2]/div/ul/li/a/strong")));
        driver.findElement(By.xpath("//div[2]/div/ul/li/a/strong")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("HelpDesk_detailView_basicAction_LBL_EDIT")));
        driver.findElement(By.id("HelpDesk_detailView_basicAction_LBL_EDIT")).click();*/


        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);


    }




    //metodo per fare una variazione di dati bancari
    public void variazioneDatiBancari() {

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        driver.findElement(By.cssSelector("button.btn.dropdown-toggle")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Dati bancari")));
        driver.findElement(By.linkText("Dati bancari")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_date_request")).sendKeys("02-11-2017");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("v")));
        driver.findElement(By.name("v")).click();


        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[5]/tbody/tr/td[2]/div/span/div/a/span")));
        driver.findElement(By.xpath("//table[5]/tbody/tr/td[2]/div/span/div/a/span")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[5]/tbody/tr/td[2]/div/span/div/div/ul/li[4]")));
        driver.findElement(By.xpath("//table[5]/tbody/tr/td[2]/div/span/div/div/ul/li[4]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[5]/tbody/tr/td[2]/div/span/div/a/span")));
        driver.findElement(By.xpath("//table[5]/tbody/tr/td[2]/div/span/div/a/span")).click();


        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[5]/tbody/tr/td[2]/div/span/div/div/ul/li[2]")));
        driver.findElement(By.xpath("//table[5]/tbody/tr/td[2]/div/span/div/div/ul/li[2]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("VariazioniAssociative_editView_fieldName_vas_iban")));
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_iban")).click();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_iban")).clear();
        driver.findElement(By.id("VariazioniAssociative_editView_fieldName_vas_iban")).sendKeys("IT79F0760103200000000641001");

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("verifyIBAN")));
        driver.findElement(By.id("verifyIBAN")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("procedi")));
        driver.findElement(By.id("procedi")).click();

        try {
            Thread.sleep(6000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//a[@id='menubar_item_Contacts']/strong")));
        driver.findElement(By.xpath("//a[@id='menubar_item_Contacts']/strong")).click();

        try {
            Thread.sleep(3500);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[3]/a/strong")));
        driver.findElement(By.xpath("//li[3]/a/strong")).click();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

       /* try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Documenti")));
        driver.findElement(By.linkText("Documenti")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("//*[@id=\"detailView\"]/div/div/div[3]/div/table/tbody/tr[3]/td[3]/a")));
        if(driver.findElement(By.linkText("//*[@id=\"detailView\"]/div/div/div[3]/div/table/tbody/tr[3]/td[3]/a")).equals("VARIAZIONE DATI BANCARI.pdf")){

            System.out.println("Il documento di variazione dati bancari è presente");

        } else {

            System.out.println("Il documento di variazione dati bancari NON È presente");


        }*/

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Posizioni SIAE")));
        driver.findElement(By.linkText("Posizioni SIAE")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")));
        driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr/td[3]")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#PosizioniSiae_detailView_fieldValue_iban > span.value")));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#PosizioniSiae_detailView_fieldValue_modalita_pagamento > span.value")));
        String iban = driver.findElement(By.cssSelector("#PosizioniSiae_detailView_fieldValue_iban > span.value")).getText();
        String modalita = driver.findElement(By.cssSelector("#PosizioniSiae_detailView_fieldValue_modalita_pagamento > span.value")).getText();

        if(iban.equals("IT79F0760103200000000641001") && modalita.equals("BONIFICO BANCARIO")){

            System.out.println("La variazione dei dati bancati è stata effettuata");

        } else {

            System.out.println("La variazione dei dati bancati NON È stata effettuata");

        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }


    //metodo per fare una variazione di numero civico su un utente registrato
    public void variazioneIndirizzo() {

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@id='rightPanel']/div[3]/div/div/div/div[2]/div/div/span/button")));
        driver.findElement(By.xpath("//div[@id='rightPanel']/div[3]/div/div/div/div[2]/div/div/span/button")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[4]/tbody/tr[3]/td[4]/span/input")));
        driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[4]/span/input")).clear();
        driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[4]/span/input")).sendKeys("33");

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='EditView']/div/span/button")));
        driver.findElement(By.xpath("//form[@id='EditView']/div/span/button")).submit();

        driver.findElement(By.name("firstname")).clear();
        driver.findElement(By.name("firstname")).sendKeys("DavideTest");
        driver.findElement(By.name("lastname")).clear();
        driver.findElement(By.name("lastname")).sendKeys("SerraTest");
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[8]/button")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("SerraTest")));
        driver.findElement(By.linkText("SerraTest")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Indirizzi")));
        driver.findElement(By.linkText("Indirizzi")).click();
        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr[1]/td[5]")));
        String civico =  driver.findElement(By.xpath("//form[@id='detailView']/div/div/div[3]/div/table/tbody/tr[1]/td[5]")).getText();

        if(civico.equals("33")){
            System.out.println("\n Variazione EFFETTUATA ");
        } else {
            System.out.println("\n NON c'è stata variazione ");
        }


        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }



    //metodo che simula l'apertura di un ticket da mobile per un associato/mandate, un utilizzatore e una rete territoriale
    public void verificaTicket() {


        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");


        String urlAssistenza = "";
        urlAssistenza = "http://crmha.BAMBOO.siae/index.php?module=CaMIA&view=List&client=0636004558&user=5002&ext=5100&ivr=BAMBOO%20IVR&date=";
        driver.get(urlAssistenza);

        String richiestaAssistenza = driver.findElement(By.xpath("//span/div/a/span")).getText();
        if (richiestaAssistenza.equals("Informazione/Assistenza")) {

            System.out.println("Il tipo di richiesta è CORRETTA");
            System.out.println(richiestaAssistenza);

        } else {

            System.out.println("La richiesta è ERRATA");
            System.out.println(richiestaAssistenza);


        }

        String modalitaAssistenza = driver.findElement(By.xpath("//tr[4]/td[2]/div/span/div/a/span")).getText();
        if (modalitaAssistenza.equals("TELEFONO")) {

            System.out.println("Il tipo di modalità è CORRETTA");
            System.out.println(modalitaAssistenza);

        } else {

            System.out.println("La modalità è ERRATA");
            System.out.println(modalitaAssistenza);


        }

        String numeroContattatoAssistenza = driver.findElement(By.xpath("//tr[5]/td[2]/div/span/div/a/span")).getText();
        if (numeroContattatoAssistenza.equals("Associato/Mandante")) {

            System.out.println("Il tipo di contatto è CORRETTO");
            System.out.println(numeroContattatoAssistenza);

        } else {

            System.out.println("Il tipo di contatto è ERRATO");
            System.out.println(numeroContattatoAssistenza);


        }

        String percorsoIvrAssistenza = driver.findElement(By.id("HelpDesk_editView_fieldName_cf_1723")).getAttribute("value");
        if (percorsoIvrAssistenza.equals("BAMBOO IVR")) {

            System.out.println("Il percorso è CORRETTO");
            System.out.println(percorsoIvrAssistenza);

        } else {

            System.out.println("Il percorso è ERRATO");
            System.out.println(percorsoIvrAssistenza);


        }

        System.out.println("\n");

        String urlMalfunzionamento = "";
        urlMalfunzionamento = "http://crmha.BAMBOO.siae/index.php?module=CaMIA&view=List&client=0636004558&user=5002&ext=5200&ivr=BAMBOO%20IVR&date=";
        driver.get(urlMalfunzionamento);

        String richiestaMalfunzionamento = driver.findElement(By.xpath("//span/div/a/span")).getText();


        if (richiestaMalfunzionamento.equals("Malfunzionamento")) {

            System.out.println("Il tipo di richiesta è CORRETTA");
            System.out.println(richiestaMalfunzionamento);

        } else {

            System.out.println("La richiesta è ERRATA");
            System.out.println(richiestaMalfunzionamento);


        }

        String modalitaMalfunzionamento = driver.findElement(By.xpath("//tr[4]/td[2]/div/span/div/a/span")).getText();
        if (modalitaMalfunzionamento.equals("TELEFONO")) {

            System.out.println("Il tipo di modalità è CORRETTA");
            System.out.println(modalitaMalfunzionamento);

        } else {

            System.out.println("La modalità è ERRATA");
            System.out.println(modalitaMalfunzionamento);


        }

        String numeroContattatoMalfunzionamento = driver.findElement(By.xpath("//tr[5]/td[2]/div/span/div/a/span")).getText();
        if (numeroContattatoMalfunzionamento.equals("Utilizzatore")) {

            System.out.println("Il tipo di contatto è CORRETTO");
            System.out.println(numeroContattatoMalfunzionamento);

        } else {

            System.out.println("Il tipo di contatto è ERRATO");
            System.out.println(numeroContattatoMalfunzionamento);


        }

        String percorsoIvrMalfunzionamento = driver.findElement(By.id("HelpDesk_editView_fieldName_cf_1723")).getAttribute("value");
        if (percorsoIvrMalfunzionamento.equals("BAMBOO IVR")) {

            System.out.println("Il percorso è CORRETTO");
            System.out.println(percorsoIvrMalfunzionamento);

        } else {

            System.out.println("Il percorso è ERRATO");
            System.out.println(percorsoIvrMalfunzionamento);


        }

        System.out.println("\n");

        String urlReteTerritoriale = "";
        urlReteTerritoriale = "http://crmha.BAMBOO.siae/index.php?module=CaMIA&view=List&client=0636004558&user=5002&ext=5000&ivr=BAMBOO%20IVR&date=";
        driver.get(urlReteTerritoriale);

        String richiestaReteTerritoriale = driver.findElement(By.xpath("//span/div/a/span")).getText();


        if (richiestaReteTerritoriale.equals("HD Rete Territoriale")) {

            System.out.println("Il tipo di richiesta è CORRETTA");
            System.out.println(richiestaReteTerritoriale);

        } else {

            System.out.println("La richiesta è ERRATA");
            System.out.println(richiestaReteTerritoriale);


        }

        String modalitaReteTerritoriale = driver.findElement(By.xpath("//tr[4]/td[2]/div/span/div/a/span")).getText();
        if (modalitaReteTerritoriale.equals("TELEFONO")) {

            System.out.println("Il tipo di modalità è CORRETTA");
            System.out.println(modalitaReteTerritoriale);

        } else {

            System.out.println("La modalità è ERRATA");
            System.out.println(modalitaReteTerritoriale);


        }


        String numeroContattatoReteTerritoriale = driver.findElement(By.xpath("//tr[5]/td[2]/div/span/div/a/span")).getText();
        if (numeroContattatoReteTerritoriale.equals("Rete Territoriale")) {

            System.out.println("Il tipo di contatto è CORRETTO");
            System.out.println(numeroContattatoReteTerritoriale);

        } else {

            System.out.println("Il tipo di contatto è ERRATO");
            System.out.println(numeroContattatoReteTerritoriale);


        }

        String percorsoIvrReteTerritoriale = driver.findElement(By.id("HelpDesk_editView_fieldName_cf_1723")).getAttribute("value");
        if (percorsoIvrReteTerritoriale.equals("BAMBOO IVR")) {

            System.out.println("Il percorso è CORRETTO");
            System.out.println(percorsoIvrReteTerritoriale);

        } else {

            System.out.println("Il percorso è ERRATO");
            System.out.println(percorsoIvrReteTerritoriale);


        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }



    //metodo che aggiunge un ticket
    public void aggiungiTickets(Customer customer) {

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        String telefonoContatto = "";
        String emailContatto = "";
        String tipologiaUtente = "";
        String tipologiaAventeDiritto = "";
        String tipoRichiesta = "";
        String repertorio = "";
        String area = "";
        String descrizione = "";
        String gruppoAssegnazione = "";


        driver.findElement(By.id("menubar_item_HelpDesk")).click();
        driver.findElement(By.id("HelpDesk_listView_basicAction_LBL_ADD_RECORD")).click();

        if (customer.getTipoRichiesta().equals("")) {
            tipoRichiesta = "Malfunzionamento";
        } else {
            tipoRichiesta = customer.getTipoRichiesta();
        }
        WebElement fieldTipoRichiesta = driver.findElement(By.name("cf_1897"));
        actions.moveToElement(fieldTipoRichiesta);
        selectFromChosen("cf_1897", tipoRichiesta);


        if (customer.getTelefono().equals("")) {
            telefonoContatto = creaTelefono();
        } else {
            telefonoContatto = customer.getTelefono();
        }
        WebElement fieldTelofono = driver.findElement(By.name("cf_1721"));
        actions.moveToElement(fieldTelofono);
        fieldTelofono.sendKeys(telefonoContatto);


        if (customer.getEmailContatto().equals("")) {
            emailContatto = creaEmail();
            driver.switchTo().window(finestraPrincipale);
        } else {
            emailContatto = customer.getEmail();
        }
        driver.findElement(By.name("cf_1753")).sendKeys(emailContatto);


        if (customer.getTipologiaUtente().equals("")) {
            tipologiaUtente = "Avente Diritto";
        } else {
            tipologiaUtente = customer.getTipologiaUtente();
        }
        WebElement fieldTipologiaUtente = driver.findElement(By.name("tt_tipologia_utente"));
        actions.moveToElement(fieldTipologiaUtente);
        selectFromChosen("tt_tipologia_utente", tipologiaUtente);


        if (customer.getTipologiaAventeDiritto().equals("")) {
            tipologiaAventeDiritto = "AUTORE";
        } else {
            tipologiaAventeDiritto = customer.getTipologiaAventeDiritto();
        }
        WebElement fieldTipologiaAventeDiritto = driver.findElement(By.name("tt_tipologia_avente_diritto"));
        actions.moveToElement(fieldTipologiaAventeDiritto);
        selectFromChosen("tt_tipologia_avente_diritto", tipologiaAventeDiritto);


        if (customer.getRepertorio().equals("")) {
            repertorio = "DOR";
        } else {
            repertorio = customer.getRepertorio();
        }
        WebElement fieldRepertorio = driver.findElement(By.name("tt_repertorio"));
        actions.moveToElement(fieldRepertorio);
        selectFromChosen("tt_repertorio", repertorio);


        if (customer.getArea().equals("")) {
            area = "Supporto a problematiche compositive/approfondimenti";
        } else {
            area = customer.getArea();
        }
        try {
            Thread.sleep(500);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@name='cf_1695']/following-sibling::div/div/ul[@class='chzn-results']/li[contains(text(), \"" + area + "\")]")));
        WebElement fieldArea = driver.findElement(By.name("cf_1695"));
        actions.moveToElement(fieldArea);
        selectFromChosen("cf_1695", area);

        if (customer.getDescrizione().equals("")) {
            descrizione = "Una descrizione qualsiasi per testare l'APERTURA di un ticket di assistenza tramite BAMBOO automatico";
        } else {
            descrizione = customer.getDescrizione();
        }
        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("cke_38_label")));
        driver.findElement(By.id("cke_38_label")).click();
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@id='cke_1_contents']/textarea")));
        driver.findElement(By.xpath("//div[@id='cke_1_contents']/textarea")).sendKeys(descrizione);
        driver.findElement(By.id("cke_38_label")).click();

        if (customer.getGruppoAssegnazione().equals("")) {
            gruppoAssegnazione = "CONTABILITA' MUSICA";
        } else {
            gruppoAssegnazione = customer.getGruppoAssegnazione();
        }
        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("tt_gruppo")));
        WebElement fieldGruppoAssegnazione = driver.findElement(By.name("tt_gruppo"));
        actions.moveToElement(fieldGruppoAssegnazione);
        selectFromChosen("tt_gruppo", gruppoAssegnazione);

       /* try {
            Thread.sleep(6000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(//button[@type='button'])[2]")));
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();


      //switch to popup mancanteL'i

        try {
            Thread.sleep(6000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[@id='Documents_popUpListView_row_1']/td[1]/input")));
        driver.findElement(By.xpath("//tr[@id='Documents_popUpListView_row_1']/td[1]/input")).click();*/



        try {
            Thread.sleep(6000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("save")));
        driver.findElement(By.name("save")).submit();

     /*   wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='detailView']/div/div/div/div/table/tbody/tr[17]/td[2]/div/span")));
        String numeroTicket = driver.findElement(By.xpath("//form[@id='detailView']/div/div/div/div/table/tbody/tr[17]/td[2]/div/span")).getText();

        driver.findElement(By.cssSelector("strong")).click();
        driver.findElement(By.name("ticket_no")).click();
        driver.findElement(By.name("ticket_no")).clear();
        driver.findElement(By.name("ticket_no")).sendKeys(numeroTicket);
        driver.findElement(By.xpath("//div[@id='listViewContents']/div[5]/div/table/tbody/tr/td[14]/button")).click();
        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[@id='HelpDesk_listView_row_1']/td[14]/div/span/a[2]/i")));
        driver.findElement(By.xpath("//tr[@id='HelpDesk_listView_row_1']/td[14]/div/span/a[2]/i")).click();

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("HelpDesk_editView_fieldName_solution")));
        driver.findElement(By.id("HelpDesk_editView_fieldName_solution")).clear();
        driver.findElement(By.id("HelpDesk_editView_fieldName_solution")).sendKeys("Descrizione per testare la CHIUSURA di un ticket di assistenza tramite BAMBOO automatico");

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("save")));
        driver.findElement(By.name("save")).submit();*/

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);


    }


    public void apriNuovaIscrizioneOffline() {
        driver.findElement(By.id("IscrizioneOffline_listView_basicAction_LBL_ADD_RECORD")).click();
    }

    public void compilaFormIscrizioneOffline(Customer customer) {
        String nazioneSedeLegale = "";
        String provinciaSedeLegale = "";
        String comuneSedeLegale = "";
        String toponimoSedeLegale = "";
        String indirizzoSedeLogale = "";
        String civicoSedeLegale = "";
        String capSedeLegale = "";
        String qualificaRappresentanteLegale = "";
        String nomeRappresentante = "";
        String cognomeRappresentante = "";
        String nome = "";
        String cognome = "";
        String sesso = "";
        String codiceFiscale = "";
        String partitaIva = "";
        String email = "";
        String telefono = "";
        String giornoNascita = "";
        String meseNascita = "";
        String annoNascita = "";
        String provinciaNascita = "";
        String comuneNascita = "";
        String provinciaNascitaSigla = "";
        String nazioneNascita = "";
        String codiceFiscaleRappresentante = "";
        String comuneNascitaRappresentante = "";
        String nazioneNascitaRappresentante = "";
        String provinciaNascitaRappresentante = "";
        String provinciaNascitaSiglaRappresentante = "";
        String giornoNascitaRappresentante = "";
        String meseNascitaRappresentante = "";
        String annoNascitaRappresentante = "";
        String sessoRappresentante = "";
        String emailRappresentante = "";
        String giornoMorte = "";
        String meseMorte = "";
        String annoMorte = "";
        String cittadinanza = "";
        String prefissoInternazionale = "";
        String numeroCellulare = "";
        String nazioneResidenza = "";
        String provinciaResidenza = "";
        String comuneResidenza = "";
        String comuneEsteroResidenza = "";
        String toponimoResidenza = "";
        String indirizzoResidenza = "";
        String civicoResidenza = "";
        String capResidenza = "";
        String nazioneDomicilio = "";
        String provinciaDomicilio = "";
        String comuneDomicilio = "";
        String comuneEsteroDomicilio = "";
        String toponimoDomicilio = "";
        String indirizzoDomicilio = "";
        String capDomicilio = "";
        String civicoDomicilio = "";

        selectFromChosen("iof_natura_giuridica", customer.getNaturaGiuridica());
        selectFromChosen("iof_tipo", customer.getTipoIscrizione());
        selectFromChosen("iof_categoria", customer.getCategoria());
        if (customer.getNome().equals("")) {
            nome = RandomStringUtils.randomAlphabetic(12).toUpperCase();
        } else {
            nome = customer.getNome();
        }
        if (customer.getCognome().equals("")) {
            cognome = RandomStringUtils.randomAlphabetic(12).toUpperCase();
        } else {
            cognome = customer.getCognome();
        }
        if (customer.getNaturaGiuridica().equals("PERSONA FISICA") || customer.getNaturaGiuridica().equals("DITTA INDIVIDUALE")) {
            if (customer.getCodiceFiscale().equals("")) {
                if (customer.getGiornoNascita().equals("")) {
                    giornoNascita = "01";
                } else {
                    giornoNascita = customer.getGiornoNascita();
                }
                if (customer.getMeseNascita().equals("")) {
                    meseNascita = "01";
                } else {
                    meseNascita = customer.getMeseNascita();
                }
                if (customer.getAnnoNascita().equals("")) {
                    annoNascita = "1980";
                } else {
                    annoNascita = customer.getAnnoNascita();
                }
                if (customer.getProvinciaNascitaSigla().equals("")) {
                    provinciaNascitaSigla = "RM";
                } else {
                    provinciaNascitaSigla = customer.getProvinciaNascitaSigla();
                }
                if (customer.getComuneNascita().equals("")) {
                    comuneNascita = "ROMA";
                } else {
                    comuneNascita = customer.getComuneNascita();
                }
                if (customer.getSesso().equals("")) {
                    sesso = "M";
                } else {
                    sesso = customer.getSesso();
                }
                codiceFiscale = creaCodiceFiscale(nome, cognome, giornoNascita, meseNascita, annoNascita, comuneNascita, provinciaNascitaSigla, sesso);
                driver.switchTo().window(finestraPrincipale);
            } else {
                codiceFiscale = customer.getCodiceFiscale();
            }
            driver.findElement(By.name("iof_cf")).sendKeys(codiceFiscale);
        } else {
            if (customer.getPartitaIva().equals("")) {
                partitaIva = creaPartitaIva();
            } else {
                partitaIva = customer.getPartitaIva();
            }
            driver.findElement(By.name("iof_cf")).sendKeys(partitaIva);
        }

        if (!customer.getNaturaGiuridica().equals("PERSONA FISICA")) {
            if (customer.getPartitaIva().equals("")) {
                partitaIva = creaPartitaIva();
            } else {
                partitaIva = customer.getPartitaIva();
            }
            driver.findElement(By.name("iof_piva")).sendKeys(partitaIva);
        }


        if (customer.getEmail().equals("")) {
            email = creaEmail();
            driver.switchTo().window(finestraPrincipale);
        } else {
            email = customer.getEmail();
        }
        driver.findElement(By.name("iof_email")).sendKeys(email);
        driver.findElement(By.xpath("//*[@id='IscrizioneOffline_editView_fieldName_iof_condizioni_adesione_siae']")).click();
        if (!customer.getNaturaGiuridica().equals("PERSONA FISICA")) {
            driver.findElement(By.xpath("//*[@id='IscrizioneOffline_editView_fieldName_iof_attivita_editoriali']")).click();
            driver.findElement(By.name("iof_indirizzo_pec")).sendKeys("mariorossi@pec.it");
        }

        if (customer.getMandatoPostumo()) {
            driver.findElement(By.id("IscrizioneOffline_editView_fieldName_iof_mandato_postumo")).click();
            //Todo: gestire la ricerca del mandato postumo.
        }
        driver.findElement(By.id("verifyCustomer")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("pageLoader")));
        if (!customer.getNaturaGiuridica().equals("PERSONA FISICA")) {
            Boolean isPresentPopupEditoriTrovati = driver.findElements(By.id("popupEditoriTrovati")).size() > 0;
            if (isPresentPopupEditoriTrovati) {
                if (driver.findElements(By.cssSelector("[type=radio][name=nuovo_cliente_scelto]")).size() > 0) {
                    driver.findElements(By.cssSelector("[type=radio][name=nuovo_cliente_scelto]")).get(0).click();
                    driver.findElement(By.id("popup_scegli_editori_scegli")).click();
                } else {
                    driver.findElement(By.id("popup_scegli_editori_prosegui")).click();
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("pageLoader")));
                }
            } else {
                if (driver instanceof JavascriptExecutor) {
                    ((JavascriptExecutor) driver)
                            .executeScript("alert('Popup non aperto');");
                }
            }
            WebElement iof_a_denominazione = driver.findElement(By.name("iof_a_denominazione"));
            actions.moveToElement(iof_a_denominazione);
            iof_a_denominazione.sendKeys(cognome.toUpperCase());
            if (customer.getTelefono().equals("")) {
                telefono = creaTelefono();
            } else {
                telefono = customer.getTelefono();
            }
            WebElement iof_a_telefono_numero = driver.findElement(By.name("iof_a_telefono_numero"));
            actions.moveToElement(iof_a_telefono_numero);
            iof_a_telefono_numero.sendKeys(telefono);

            WebElement iof_a_vat_number = driver.findElement(By.name("iof_a_vat_number"));
            actions.moveToElement(iof_a_vat_number);
            iof_a_vat_number.sendKeys(customer.getVatNumber());

            if (customer.getIscrizioneVies().equals(true)) {
                WebElement iscrizioneVies = driver.findElement(By.cssSelector("[type=checkbox][name=iof_a_vies]"));
                actions.moveToElement(iscrizioneVies);
                iscrizioneVies.click();
            }
            if (customer.getIscrizioneAttivitaEconomica().equals(true)) {
                WebElement iscrizioneAttivitaEconomica = driver.findElement(By.cssSelector("[type=checkbox][name=iof_a_attivita_economica]"));
                actions.moveToElement(iscrizioneAttivitaEconomica);
                iscrizioneAttivitaEconomica.click();
            }
            if (customer.getNazioneSedeLegale().equals("")) {
                nazioneSedeLegale = "ITALIA";
            } else {
                nazioneSedeLegale = customer.getNazioneSedeLegale();
            }
            WebElement fieldNazioneSedeLegale = driver.findElement(By.name("iof_a_nazione_sl"));
            actions.moveToElement(fieldNazioneSedeLegale);
            selectFromChosen("iof_a_nazione_sl", nazioneSedeLegale);

            if (nazioneSedeLegale.equals("ITALIA")) {
                if (customer.getProvinciaNascita().equals("")) {
                    provinciaSedeLegale = "ROMA";
                } else {
                    provinciaSedeLegale = customer.getProvinciaNascita();
                }
                WebElement fieldProvinciaSedeLegale = driver.findElement(By.name("iof_a_provincia_sl"));
                actions.moveToElement(fieldProvinciaSedeLegale);
                selectFromChosen("iof_a_provincia_sl", provinciaSedeLegale);

                if (customer.getComuneSedeLegale().equals("")) {
                    comuneSedeLegale = "ROMA";
                } else {
                    comuneSedeLegale = customer.getComuneSedeLegale();
                }
                try {
                    Thread.sleep(500);//1000 milliseconds is one second.
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@name='iof_a_comune_sl']/following-sibling::div/div/ul[@class='chzn-results']/li[contains(text(), \"" + comuneSedeLegale + "\")]")));
                WebElement fieldComuneSedeLegale = driver.findElement(By.name("iof_a_comune_sl"));
                actions.moveToElement(fieldComuneSedeLegale);
                selectFromChosen("iof_a_comune_sl", comuneSedeLegale);
            } else {
                if (!customer.getComuneEsteroSedeLegale().equals("")) {
                    WebElement fieldComuneEsteroSedeLegale = driver.findElement(By.name("iof_a_comune_estero_sl"));
                    actions.moveToElement(fieldComuneEsteroSedeLegale);
                    fieldComuneEsteroSedeLegale.sendKeys(customer.getComuneSedeLegale());
                }
            }
            if (customer.getToponimoSedeLegale().equals("")) {
                toponimoSedeLegale = "VIA DELLE";
            } else {
                toponimoSedeLegale = customer.getToponimoSedeLegale();
            }
            WebElement fieldToponimoSedeLegale = driver.findElement(By.name("iof_a_toponimo_sl"));
            actions.moveToElement(fieldToponimoSedeLegale);
            fieldToponimoSedeLegale.sendKeys(toponimoSedeLegale);

            if (customer.getIndirizzoSedeLegale().equals("")) {
                indirizzoSedeLogale = "CINCIE";
            } else {
                indirizzoSedeLogale = customer.getIndirizzoSedeLegale();
            }
            WebElement fieldIndirizzoSedeLegale = driver.findElement(By.name("iof_a_indirizzo_sl"));
            actions.moveToElement(fieldIndirizzoSedeLegale);
            fieldIndirizzoSedeLegale.sendKeys(indirizzoSedeLogale);

            if (customer.getCivicoSedeLegale().equals("")) {
                civicoSedeLegale = "20";
            } else {
                civicoSedeLegale = customer.getCivicoSedeLegale();
            }
            WebElement fieldCivicoSedeLegale = driver.findElement(By.name("iof_a_civico_sl"));
            actions.moveToElement(fieldCivicoSedeLegale);
            fieldCivicoSedeLegale.sendKeys(civicoSedeLegale);

            if (customer.getCapSedeLegale().equals("")) {
                capSedeLegale = "00169";
            } else {
                capSedeLegale = customer.getCapSedeLegale();
            }
            WebElement fieldCapSedeLegale = driver.findElement(By.name("iof_a_cap_sl"));
            actions.moveToElement(fieldCapSedeLegale);
            fieldCapSedeLegale.sendKeys(capSedeLegale);

            if (customer.getQualificaRappresentanteLegale().equals("")) {
                qualificaRappresentanteLegale = "RAPPRESENTANTE LEGALE";
            } else {
                qualificaRappresentanteLegale = customer.getQualificaRappresentanteLegale();
            }
            WebElement fieldQualificaRappresentanteLegale = driver.findElement(By.name("iof_a_qualifica_rl"));
            actions.moveToElement(fieldQualificaRappresentanteLegale);
            selectFromChosen("iof_a_qualifica_rl", qualificaRappresentanteLegale);

            if (customer.getNomeRappresentante().equals("")) {
                nomeRappresentante = RandomStringUtils.randomAlphabetic(12).toUpperCase();
            } else {
                nomeRappresentante = customer.getNomeRappresentante().toUpperCase();
            }
            WebElement fieldNomeRappresentante = driver.findElement(By.name("iof_a_nome_rl"));
            actions.moveToElement(fieldNomeRappresentante);
            fieldNomeRappresentante.sendKeys(nomeRappresentante);

            if (customer.getCognomeRappresentante().equals("")) {
                cognomeRappresentante = RandomStringUtils.randomAlphabetic(12).toUpperCase();
            } else {
                cognomeRappresentante = customer.getCognomeRappresentante().toUpperCase();
            }
            WebElement fieldCognomeRappresentante = driver.findElement(By.name("iof_a_cognome_rl"));
            actions.moveToElement(fieldCognomeRappresentante);
            fieldCognomeRappresentante.sendKeys(cognomeRappresentante);

            if (customer.getGiornoNascitaRappresentante().equals("")) {
                giornoNascitaRappresentante = "01";
            } else {
                giornoNascitaRappresentante = customer.getGiornoNascitaRappresentante();
            }

            if (customer.getMeseNascitaRappresentante().equals("")) {
                meseNascitaRappresentante = "01";
            } else {
                meseNascitaRappresentante = customer.getMeseNascitaRappresentante();
            }
            if (customer.getAnnoNascitaRappresentante().equals("")) {
                annoNascitaRappresentante = "1980";
            } else {
                annoNascitaRappresentante = customer.getAnnoNascitaRappresentante();
            }
            WebElement fieldDataNascitaRappresentante = driver.findElement(By.name("iof_a_data_nascita_rl"));
            actions.moveToElement(fieldDataNascitaRappresentante);
            fieldDataNascitaRappresentante.sendKeys(giornoNascitaRappresentante + "-" + meseNascitaRappresentante + "-" + annoNascitaRappresentante);
            fieldDataNascitaRappresentante.click();

            actions.moveToElement(fieldCognomeRappresentante);
            fieldCognomeRappresentante.click();

            if (customer.getProvinciaNascitaSiglaRappresentante().equals("")) {
                provinciaNascitaSiglaRappresentante = "RM";
            } else {
                provinciaNascitaSiglaRappresentante = customer.getProvinciaNascitaSiglaRappresentante();
            }

            if (customer.getProvinciaNascitaRappresentante().equals("")) {
                provinciaNascitaRappresentante = "ROMA";
            } else {
                provinciaNascitaRappresentante = customer.getProvinciaNascitaRappresentante();
            }
            if (customer.getComuneNascitaRappresentante().equals("")) {
                comuneNascitaRappresentante = "ROMA";
            } else {
                comuneNascitaRappresentante = customer.getComuneNascitaRappresentante();
            }
            if (customer.getSessoRappresentante().equals("")) {
                sessoRappresentante = "M";
            } else {
                sessoRappresentante = customer.getSessoRappresentante();
            }
            if (customer.getCodiceFiscaleRappresentante().equals("")) {
                codiceFiscaleRappresentante = creaCodiceFiscale(nomeRappresentante, cognomeRappresentante, giornoNascitaRappresentante, meseNascitaRappresentante, annoNascitaRappresentante, comuneNascitaRappresentante, provinciaNascitaSiglaRappresentante, sessoRappresentante);
                driver.switchTo().window(finestraPrincipale);
            } else {
                codiceFiscaleRappresentante = customer.getCodiceFiscaleRappresentante();
            }
            WebElement fieldCodiceFiscaleRappresentante = driver.findElement(By.name("iof_a_cf_rl"));
            actions.moveToElement(fieldCodiceFiscaleRappresentante);
            fieldCodiceFiscaleRappresentante.sendKeys(codiceFiscaleRappresentante);

            if (customer.getNazioneNascitaRappresentante().equals("")) {
                nazioneNascitaRappresentante = "ITALIA";
            } else {
                nazioneNascitaRappresentante = customer.getNazioneNascitaRappresentante();
            }
            WebElement fieldNazioneNascitaRappresentante = driver.findElement(By.name("iof_a_nazione_rl"));
            actions.moveToElement(fieldNazioneNascitaRappresentante);
            selectFromChosen("iof_a_nazione_rl", nazioneNascitaRappresentante);
            if (nazioneNascitaRappresentante.equals("ITALIA")) {
                WebElement fieldProvinciaNascitaRappresentante = driver.findElement(By.name("iof_a_provincia_rl"));
                actions.moveToElement(fieldProvinciaNascitaRappresentante);
                selectFromChosen("iof_a_provincia_rl", provinciaNascitaRappresentante);
                //attende che sia presente il valore
                wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@name='iof_a_comune_rl']/following-sibling::div/div/ul[@class='chzn-results']/li[contains(text(), \"" + comuneNascitaRappresentante + "\")]")));
                WebElement fieldComuneNascitaRappresentante = driver.findElement(By.name("iof_a_comune_rl"));
                actions.moveToElement(fieldComuneNascitaRappresentante);
                selectFromChosen("iof_a_comune_rl", comuneNascitaRappresentante);
            } else {
                if (!customer.getComuneEsteroRappresentanteLegale().equals("")) {
                    WebElement fieldComuneEsteroRappresentanteLegale = driver.findElement(By.name("iof_a_comune_estero_rl"));
                    actions.moveToElement(fieldComuneEsteroRappresentanteLegale);
                    fieldComuneEsteroRappresentanteLegale.sendKeys(customer.getComuneEsteroRappresentanteLegale());
                }
            }
            WebElement fieldSessoRappresentante = driver.findElement(By.name("iof_a_sesso_rl"));
            actions.moveToElement(fieldSessoRappresentante);
            selectFromChosen("iof_a_sesso_rl", sessoRappresentante);

            if (customer.getEmailRappresentante().equals("")) {
                emailRappresentante = creaEmail();
                driver.switchTo().window(finestraPrincipale);
            } else {
                emailRappresentante = customer.getEmailRappresentante();
            }
            WebElement fieldEmailRappresentante = driver.findElement(By.cssSelector("[name=iof_a_email_rl]"));
            actions.moveToElement(fieldEmailRappresentante);
            fieldEmailRappresentante.sendKeys(emailRappresentante);


        } else {
            Boolean isPresentPopupPosizioniPregresse = driver.findElements(By.cssSelector(".ui-dialog")).size() > 0;
            if (isPresentPopupPosizioniPregresse.equals(true)) {
                WebElement popupServizioRegistrabilita = driver.findElements(By.cssSelector(".ui-dialog")).get(0);
                logger.info("Si è aperto il popup generato dal servizio di iscrivibilità.");
                try {
                    List<WebElement> pulsanti = popupServizioRegistrabilita.findElements(By.cssSelector("span.ui-button-text"));
                    boolean found = false;
                    WebElement pulsanteIscriviComunque = null;
                    if (pulsanti.size() > 0) {
                        for (WebElement pulsante : pulsanti) {
                            if (pulsante.getAttribute("innerHTML").equals("Iscrivi comunque")) {
                                found = true;
                                pulsanteIscriviComunque = pulsante;
                                break;
                            }
                        }
                    }
                    if (!found) {
                        throw new Exception("pulsante non trovato");
                    } else {
                        pulsanteIscriviComunque.click();
                        logger.info("Premuto il pulsante 'Iscrivi Comunque'");
                        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("pageLoader")));
                        try {
                            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//th[contains(text(), 'Ex iscritto - Informazioni posizione cancellata')]")));
                        } catch (Exception e) {
                            logger.severe("Non si è aperto il blocco della posizione pregressa");
                            driver.close();
                            return;
                        }
                        WebElement fieldNumeroPosizionePregressa = driver.findElement(By.name("iof_vp_numero"));
                        actions.moveToElement(fieldNumeroPosizionePregressa);
                        if (fieldNumeroPosizionePregressa.getAttribute("value").equals("")) {
                            logger.severe("Non compare il numero della posizione pregressa");
                        } else {
                            logger.info("Il numero della posizione pregressa è: " + fieldNumeroPosizionePregressa.getAttribute("value"));
                        }
                        WebElement fieldSezioniPosizionePregressa = driver.findElement(By.name("iof_vp_sezioni"));
                        actions.moveToElement(fieldSezioniPosizionePregressa);
                        if (fieldSezioniPosizionePregressa.getAttribute("value").equals("")) {
                            logger.info("Non sono valorizzate le sezioni nella posizione pregressa");
                        } else {
                            logger.info("Le sezioni relative alla posizione pregressa sono: " + fieldSezioniPosizionePregressa.getAttribute("value"));
                        }
                        WebElement fieldCategoriaPosizionePregressa = driver.findElement(By.name("iof_vp_categoria"));
                        actions.moveToElement(fieldCategoriaPosizionePregressa);
                        if (fieldCategoriaPosizionePregressa.getAttribute("value").equals("")) {
                            logger.info("Non è valorizzata la categoria della posizione pregressa");
                        } else {
                            logger.info("La categoria della posizione pregressa è: " + fieldCategoriaPosizionePregressa.getAttribute("value"));
                        }
                        WebElement fieldTipoIscrizionePosizionePregressa = driver.findElement(By.name("iof_vp_tipo_iscrizione"));
                        actions.moveToElement(fieldTipoIscrizionePosizionePregressa);
                        if (fieldTipoIscrizionePosizionePregressa.getAttribute("value").equals("")) {
                            logger.info("Non è valorizzata la tipologia di iscrizione della posizione pregressa");
                        } else {
                            logger.info("La tipologia di iscrizione della posizione pregressa è: " + fieldTipoIscrizionePosizionePregressa.getAttribute("value"));
                        }
                        WebElement fieldDataIscrizionePosizionePregressa = driver.findElement(By.name("iof_vp_data_iscrizione"));
                        actions.moveToElement(fieldDataIscrizionePosizionePregressa);
                        if (fieldCategoriaPosizionePregressa.getAttribute("value").equals("")) {
                            logger.info("Non è valorizata la data di iscrizione della posizione pregressa");
                        } else {
                            logger.info("La data di iscrizione della posizione pregressa è " + fieldDataIscrizionePosizionePregressa.getAttribute("value"));
                        }
                        WebElement fieldDataCancellazionePosizionePregressa = driver.findElement(By.name("iof_vp_data_cancellazione"));
                        actions.moveToElement(fieldDataCancellazionePosizionePregressa);
                        if (fieldDataCancellazionePosizionePregressa.getAttribute("value").equals("")) {
                            logger.info("Non è valorizzata la data di cancellazione della posizione pregressa");
                        } else {
                            logger.info("La data di cancellazione della posizione pregressa è: " + fieldDataCancellazionePosizionePregressa.getAttribute("value"));
                        }
                        WebElement fieldMotivoCancellazionePosizionePregressa = driver.findElement(By.name("iof_vp_motivo_cancellazione"));
                        actions.moveToElement(fieldMotivoCancellazionePosizionePregressa);
                        if (fieldMotivoCancellazionePosizionePregressa.getAttribute("value").equals("")) {
                            logger.info("Non è valorizzato il motivo di cancellazione della posizione pregressa");
                        } else {
                            logger.info("Il motivo di cancellazione della posizione pregressa è: " + fieldMotivoCancellazionePosizionePregressa.getAttribute("value"));
                        }
                        WebElement fieldSaldoQuotaMorositaPosizionePregressa = driver.findElement(By.name("iof_saldo_morosita"));
                        actions.moveToElement(fieldSaldoQuotaMorositaPosizionePregressa);
                        if (fieldSaldoQuotaMorositaPosizionePregressa.getAttribute("value").equals("")) {
                            logger.info("Non è valorizzato il saldo della quota di morosità della posizione pregressa");
                        } else {
                            logger.info("Il saldo della quota di morosità della posizione pregressa è: " + fieldSaldoQuotaMorositaPosizionePregressa);
                        }
                        WebElement fieldNome = driver.findElement(By.name("iof_nome"));
                        actions.moveToElement(fieldNome);
                        if (fieldNome.getAttribute("value").equals("")) {
                            logger.severe("Il campo Nome non è valorizzato. Il BAMBOO finisce qui.");
                            driver.close();
                            return;
                        }
                        WebElement fieldCognome = driver.findElement(By.name("iof_cognome"));
                        actions.moveToElement(fieldCognome);
                        if (fieldCognome.getAttribute("value").equals("")) {
                            logger.severe("Il campo cognome non è valorizzato. Il BAMBOO finisce qui.");
                            driver.close();
                            return;
                        }
                        WebElement fieldDataNascita = driver.findElement(By.name("iof_nascita_data"));
                        actions.moveToElement(fieldDataNascita);
                        if (fieldDataNascita.getAttribute("value").equals("")) {
                            logger.severe("Il campo data di nascita non è valorizzato. Il BAMBOO finisce qui.");
                            driver.close();
                            return;
                        }
                        if (!(customer.getGiornoMorte().equals("") && customer.getMeseMorte().equals("") && customer.getAnnoMorte().equals(""))) {
                            WebElement fieldDataMorte = driver.findElement(By.name("iof_morte_data"));
                            actions.moveToElement(fieldDataMorte);
                            fieldDataMorte.sendKeys(giornoMorte + "-" + meseMorte + "-" + annoMorte);
                            fieldCognome.click();
                        }
                    }

                } catch (Exception e) {
                    logger.info("Il pulsante 'Iscrivi comunque non è presente. Il BAMBOO finisce qui.");
                    driver.close();
                    return;
                }
            } else {
                WebElement fieldNome = driver.findElement(By.name("iof_nome"));
                actions.moveToElement(fieldNome);
                fieldNome.sendKeys(nome);

                WebElement fieldCognome = driver.findElement(By.name("iof_cognome"));
                actions.moveToElement(fieldCognome);
                fieldCognome.sendKeys(cognome);

                WebElement fieldDataNascita = driver.findElement(By.name("iof_nascita_data"));
                actions.moveToElement(fieldDataNascita);
                fieldDataNascita.sendKeys(giornoNascita + "-" + meseNascita + "-" + annoNascita);
                fieldCognome.click();

                if (!(customer.getGiornoMorte().equals("") && customer.getMeseMorte().equals("") && customer.getAnnoMorte().equals(""))) {
                    WebElement fieldDataMorte = driver.findElement(By.name("iof_morte_data"));
                    actions.moveToElement(fieldDataMorte);
                    fieldDataMorte.sendKeys(giornoMorte + "-" + meseMorte + "-" + annoMorte);
                    fieldCognome.click();
                }
                if (customer.getNazioneNascita().equals("")) {
                    nazioneNascita = "ITALIA";
                } else if (customer.getNazioneNascita().equals("-")) {
                    nazioneNascita = "";
                } else {
                    nazioneNascita = customer.getNazioneNascita();
                }
                WebElement fieldStatoNascita = driver.findElement(By.name("iof_nascita_stato"));
                actions.moveToElement(fieldStatoNascita);
                selectFromChosen("iof_nascita_stato", nazioneNascita);

                if (customer.getProvinciaNascita().equals("")) {
                    provinciaNascita = "ROMA";
                } else if (customer.getProvinciaNascita().equals("-")) {
                    provinciaNascita = "";
                } else {
                    provinciaNascita = customer.getProvinciaNascita();
                }


                if (nazioneNascita.equals("ITALIA")) {
                    WebElement fieldProvinciaNascita = driver.findElement(By.name("iof_nascita_provincia"));
                    actions.moveToElement(fieldProvinciaNascita);
                    selectFromChosen("iof_nascita_provincia", provinciaNascita);
                    wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@name='iof_nascita_comune']/following-sibling::div/div/ul[@class='chzn-results']/li[contains(text(), \"" + comuneNascita + "\")]")));
                    WebElement fieldComuneNascita = driver.findElement(By.name("iof_nascita_comune"));
                    actions.moveToElement(fieldComuneNascita);
                    selectFromChosen("iof_nascita_comune", comuneNascita);
                } else {
                    WebElement fieldComuneEsteroNascita = driver.findElement(By.name("iof_nascita_citta_estera"));
                    actions.moveToElement(fieldComuneEsteroNascita);
                    fieldComuneEsteroNascita.sendKeys(comuneNascita);
                }


                if (customer.getCittadinanza().equals("")) {
                    cittadinanza = "ITALIA";
                } else if (customer.getCittadinanza().equals("-")) {
                    cittadinanza = "-";
                } else {
                    cittadinanza = customer.getCittadinanza();
                }
                WebElement fieldCittadinanza = driver.findElement(By.name("iof_cittadinanza"));
                actions.moveToElement(fieldCittadinanza);
                selectFromChosen("iof_cittadinanza", cittadinanza);

                WebElement fieldSesso = driver.findElement(By.name("iof_sesso"));
                actions.moveToElement(fieldSesso);
                selectFromChosen("iof_sesso", sesso);

                if (customer.getPrefissoInternazionale().equals("")) {
                    prefissoInternazionale = "0039";
                } else if (customer.getPrefissoInternazionale().equals("-")) {
                    prefissoInternazionale = "";
                } else {
                    prefissoInternazionale = customer.getPrefissoInternazionale();
                }
                WebElement fieldPrefissoIntenazionale = driver.findElement(By.name("iof_cell_prefisso"));
                actions.moveToElement(fieldPrefissoIntenazionale);
                selectFromChosen("iof_cell_prefisso", prefissoInternazionale);

                if (customer.getNumeroCellulare().equals("")) {
                    numeroCellulare = "328978197";
                } else if (customer.getNumeroCellulare().equals("-")) {
                    numeroCellulare = "";
                } else {
                    numeroCellulare = customer.getNumeroCellulare();
                }
                WebElement fieldNumeroCellulare = driver.findElement(By.name("iof_cell_numero"));
                actions.moveToElement(fieldNumeroCellulare);
                fieldNumeroCellulare.sendKeys(numeroCellulare);
                if (customer.getForzaturaNumeroCellulare().equals(true)) {
                    WebElement fieldForzaturaCodiceFiscale = driver.findElement(By.cssSelector("[type=checkbox][name=iof_cell_forzatura]"));
                    actions.moveToElement(fieldForzaturaCodiceFiscale);
                    fieldForzaturaCodiceFiscale.click();
                }
                if (customer.getInvalido().equals(true)) {
                    WebElement fieldInvalido = driver.findElement(By.cssSelector("[type=checkbox][name=iof_invalido]"));
                    actions.moveToElement(fieldInvalido);
                    fieldInvalido.click();
                }
                if (customer.getNazioneResidenza().equals("")) {
                    nazioneResidenza = "ITALIA";
                } else if (customer.getNazioneResidenza().equals("-")) {
                    nazioneResidenza = "";
                } else {
                    nazioneResidenza = customer.getNazioneResidenza();
                }
                WebElement fieldNazioneResidenza = driver.findElement(By.name("iof_residenza_stato"));
                actions.moveToElement(fieldNazioneResidenza);
                selectFromChosen("iof_residenza_stato", nazioneResidenza);

                if (customer.getProvinciaResidenza().equals("")) {
                    provinciaResidenza = "ROMA";
                } else if (customer.getProvinciaResidenza().equals("-")) {
                    provinciaResidenza = "";
                } else {
                    provinciaResidenza = customer.getProvinciaResidenza();
                }
                WebElement fieldProvinciaResidenza = driver.findElement(By.name("iof_residenza_provincia"));
                actions.moveToElement(fieldProvinciaResidenza);
                selectFromChosen("iof_residenza_provincia", provinciaResidenza);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (customer.getComuneResidenza().equals("")) {
                    comuneResidenza = "ROMA";
                } else if (customer.getComuneResidenza().equals("-")) {
                    comuneResidenza = "";
                } else {
                    comuneResidenza = customer.getComuneResidenza();
                }
                WebElement fieldComuneResidenza = driver.findElement(By.name("iof_residenza_comune"));
                actions.moveToElement(fieldComuneResidenza);
                selectFromChosen("iof_residenza_comune", comuneResidenza);

                if (!customer.getComuneEsteroResidenza().equals("")) {
                    WebElement fieldComuneEsteroResidenza = driver.findElement(By.name("iof_residenza_citta_estera"));
                    actions.moveToElement(fieldComuneEsteroResidenza);
                    fieldComuneEsteroResidenza.sendKeys(comuneEsteroResidenza);
                }

                if (customer.getToponimoResidenza().equals("")) {
                    toponimoResidenza = "VIA DELLE";
                } else if (customer.getToponimoResidenza().equals("-")) {
                    toponimoResidenza = "";
                } else {
                    toponimoResidenza = customer.getToponimoResidenza();
                }
                WebElement fieldToponimoResidenza = driver.findElement(By.name("iof_residenza_toponimo"));
                actions.moveToElement(fieldToponimoResidenza);
                fieldToponimoResidenza.sendKeys(toponimoResidenza);

                if (customer.getIndirizzoResidenza().equals("")) {
                    indirizzoResidenza = "CINCIE";
                } else if (customer.getIndirizzoResidenza().equals("-")) {
                    indirizzoResidenza = "";
                } else {
                    indirizzoResidenza = customer.getIndirizzoResidenza();
                }
                WebElement fieldIndirizzoResidenza = driver.findElement(By.name("iof_residenza_indirizzo"));
                actions.moveToElement(fieldIndirizzoResidenza);
                fieldIndirizzoResidenza.sendKeys(indirizzoResidenza);

                if (customer.getCivicoResidenza().equals("")) {
                    civicoResidenza = "20";
                } else if (customer.getCivicoResidenza().equals("-")) {
                    civicoResidenza = "";
                } else {
                    civicoResidenza = customer.getCivicoResidenza();
                }
                WebElement fieldCivicoResidenza = driver.findElement(By.name("iof_residenza_n_civico"));
                actions.moveToElement(fieldCivicoResidenza);
                fieldCivicoResidenza.sendKeys(civicoResidenza);
                if (customer.getCapResidenza().equals("")) {
                    capResidenza = "00000";
                } else if (customer.getCapResidenza().equals("-")) {
                    capResidenza = "";
                } else {
                    capResidenza = customer.getCapResidenza();
                }
                WebElement fieldCapResidenza = driver.findElement(By.name("iof_residenza_cap"));
                actions.moveToElement(fieldCapResidenza);
                fieldCapResidenza.sendKeys(capResidenza);
                if (customer.getDomicilioDiversoDaResidenza().equals(true)) {
                    WebElement fieldDomicilioDiversoDaResidenza = driver.findElement(By.cssSelector("[type=checkbox][name=iof_aggiungi_domicilio]"));
                    actions.moveToElement(fieldDomicilioDiversoDaResidenza);
                    fieldDomicilioDiversoDaResidenza.click();
                    if (customer.getNazioneDomicilio().equals("")) {
                        nazioneDomicilio = "ITALIA";
                    } else if (customer.getNazioneDomicilio().equals("-")) {
                        nazioneDomicilio = "";
                    } else {
                        nazioneDomicilio = customer.getNazioneDomicilio();
                    }
                    WebElement fieldNazioneDomicilio = driver.findElement(By.name("iof_domicilio_stato"));
                    actions.moveToElement(fieldNazioneDomicilio);
                    selectFromChosen("iof_domicilio_stato", nazioneDomicilio);

                    if (customer.getProvinciaDomicilio().equals("")) {
                        provinciaDomicilio = "ROMA";
                    } else if (customer.getProvinciaDomicilio().equals("-")) {
                        provinciaDomicilio = "";
                    } else {
                        provinciaDomicilio = customer.getProvinciaDomicilio();
                    }
                    WebElement fieldProvinciaDomicilio = driver.findElement(By.name("iof_domicilio_provincia"));
                    actions.moveToElement(fieldProvinciaDomicilio);
                    selectFromChosen("iof_domicilio_provincia", provinciaDomicilio);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (customer.getComuneDomicilio().equals("")) {
                        comuneDomicilio = "ROMA";
                    } else if (customer.getComuneDomicilio().equals("-")) {
                        comuneDomicilio = "";
                    } else {
                        comuneDomicilio = customer.getComuneDomicilio();
                    }
                    WebElement fieldComuneDomicilio = driver.findElement(By.name("iof_domicilio_comune"));
                    actions.moveToElement(fieldComuneDomicilio);
                    selectFromChosen("iof_domicilio_comune", comuneDomicilio);

                    if (!customer.getComuneEsteroDomicilio().equals("")) {
                        WebElement fieldComuneEsteroDomicilio = driver.findElement(By.name("iof_domicilio_citta_estera"));
                        actions.moveToElement(fieldComuneEsteroDomicilio);
                        fieldComuneEsteroDomicilio.sendKeys(comuneEsteroDomicilio);
                    }

                    if (customer.getToponimoDomicilio().equals("")) {
                        toponimoDomicilio = "VIA";
                    } else if (customer.getToponimoDomicilio().equals("-")) {
                        toponimoDomicilio = "";
                    } else {
                        toponimoDomicilio = customer.getToponimoDomicilio();
                    }
                    WebElement fieldToponimoDomicilio = driver.findElement(By.name("iof_domicilio_toponimo"));
                    actions.moveToElement(fieldToponimoDomicilio);
                    fieldToponimoDomicilio.sendKeys(toponimoDomicilio);

                    if (customer.getIndirizzoDomicilio().equals("")) {
                        indirizzoDomicilio = "PELLEGRINO MATTEUCCI";
                    } else if (customer.getIndirizzoDomicilio().equals("-")) {
                        indirizzoDomicilio = "";
                    } else {
                        indirizzoDomicilio = customer.getIndirizzoDomicilio();
                    }
                    WebElement fieldIndirizzoDomicilio = driver.findElement(By.name("iof_domicilio_indirizzo"));
                    actions.moveToElement(fieldIndirizzoDomicilio);
                    fieldIndirizzoDomicilio.sendKeys(indirizzoDomicilio);

                    if (customer.getCivicoDomicilio().equals("")) {
                        civicoDomicilio = "134";
                    } else if (customer.getCivicoDomicilio().equals("-")) {
                        civicoDomicilio = "";
                    } else {
                        civicoDomicilio = customer.getCivicoDomicilio();
                    }
                    WebElement fieldCivicoDomicilio = driver.findElement(By.name("iof_domicilio_n_civico"));
                    actions.moveToElement(fieldCivicoDomicilio);
                    fieldCivicoDomicilio.sendKeys(civicoDomicilio);
                    if (customer.getCapDomicilio().equals("")) {
                        capDomicilio = "00000";
                    } else if (customer.getCapDomicilio().equals("-")) {
                        capDomicilio = "";
                    } else {
                        capDomicilio = customer.getCapDomicilio();
                    }
                    WebElement fieldCapDomicilio = driver.findElement(By.name("iof_domicilio_cap"));
                    actions.moveToElement(fieldCapDomicilio);
                    fieldCapDomicilio.sendKeys(capDomicilio);
                    WebElement pulsanteVerificaIndirizzi = driver.findElement(By.id("verifyAddress"));
                    actions.moveToElement(pulsanteVerificaIndirizzi);
                    pulsanteVerificaIndirizzi.click();
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".ui-dialog")));
                    WebElement popupSuggerimenti = driver.findElement(By.cssSelector(".ui-dialog"));
                    List<WebElement> tabelleSuggerimenti = popupSuggerimenti.findElements(By.cssSelector("table.iscrizione-offline-suggerimenti-indirizzo"));
                    WebElement tabellaResidenza = tabelleSuggerimenti.get(0);
                    List<WebElement> suggerimentiResidenza = tabellaResidenza.findElements(By.cssSelector("[type=radio][name=suggerimento_residenza]"));
                    WebElement suggerimentoResidenza = suggerimentiResidenza.get(0);
                    suggerimentoResidenza.click();
                    WebElement tabellaDomicilio = tabelleSuggerimenti.get(1);
                    List<WebElement> suggerimentiDomicilio = tabellaDomicilio.findElements(By.cssSelector("[type=radio][name=suggerimento_domicilio]"));
                    WebElement suggerimentoDomicilio = suggerimentiDomicilio.get(0);
                    suggerimentoDomicilio.click();
                    WebElement popupSuggerimentiOk = popupSuggerimenti.findElements(By.cssSelector(".ui-button-text")).get(0);
                    popupSuggerimentiOk.click();
                } else {
                    WebElement pulsanteVerificaIndirizzi = driver.findElement(By.id("verifyAddress"));
                    actions.moveToElement(pulsanteVerificaIndirizzi);
                    pulsanteVerificaIndirizzi.click();
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".ui-dialog")));
                    WebElement popupSuggerimenti = driver.findElement(By.cssSelector(".ui-dialog"));
                    List<WebElement> tabelleSuggerimenti = popupSuggerimenti.findElements(By.cssSelector("table.iscrizione-offline-suggerimenti-indirizzo"));
                    WebElement tabellaResidenza = tabelleSuggerimenti.get(0);
                    List<WebElement> suggerimentiResidenza = tabellaResidenza.findElements(By.cssSelector("[type=radio][name=suggerimento_residenza]"));
                    WebElement suggerimentoResidenza = suggerimentiResidenza.get(0);
                    suggerimentoResidenza.click();
                    WebElement popupSuggerimentiOk = popupSuggerimenti.findElements(By.cssSelector(".ui-button-text")).get(0);
                    popupSuggerimentiOk.click();
                }

            }
        }


        String tipologiaConto;
        if (customer.getTipologiaConto().equals("")) {
            tipologiaConto = "Italiano";
        } else if (customer.getTipologiaConto().equals("-")) {
            tipologiaConto = "";
        } else {
            tipologiaConto = customer.getTipologiaConto();
        }
        WebElement fieldTipologiaConto = driver.findElement(By.name("iof_tipologia_conto"));
        actions.moveToElement(fieldTipologiaConto);
        selectFromChosen("iof_tipologia_conto", tipologiaConto);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String modalitaAccredito;
        if (customer.getModalitaAccredito().equals("")) {
            modalitaAccredito = "BONIFICO BANCARIO";
        } else if (customer.getModalitaAccredito().equals("-")) {
            modalitaAccredito = "";
        } else {
            modalitaAccredito = customer.getModalitaAccredito();
        }
        WebElement fieldModalitaAccredito = driver.findElement(By.name("iof_modalita_pagamento"));
        actions.moveToElement(fieldModalitaAccredito);
        selectFromChosen("iof_modalita_pagamento", modalitaAccredito);
        if (!modalitaAccredito.equals("BONIFICO DOMICILIATO")) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("iof_iban")));
            String iban;
            if (customer.getIban().equals("")) {
                iban = "IT73K0301503200000002668243";
            } else if (customer.getIban().equals("-")) {
                iban = "";
            } else {
                iban = customer.getIban();
            }
            WebElement fieldIban = driver.findElement(By.name("iof_iban"));
            actions.moveToElement(fieldIban);
            fieldIban.sendKeys(iban);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("verifyIBAN")));
            WebElement pulsanteVerificaIban = driver.findElement(By.id("verifyIBAN"));
            pulsanteVerificaIban.click();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("pageLoader")));
            String dataInizioTutela = "";
            DateTimeFormatter formattatore = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            if (customer.getDataInizioTutela().equals("")) {
                LocalDate oggi = LocalDate.now();
                dataInizioTutela = formattatore.format(oggi);
            } else if (customer.getDataInizioTutela().equals("-")) {
                dataInizioTutela = "";
            } else {
                dataInizioTutela = customer.getDataInizioTutela();
            }
            WebElement fieldDataInizioTutela = driver.findElement(By.name("iof_data_competenza"));
            actions.moveToElement(fieldDataInizioTutela);
            fieldDataInizioTutela.sendKeys(dataInizioTutela);

            WebElement fieldEstremiPagamento = driver.findElement(By.name("iof_estremi"));
            actions.moveToElement(fieldEstremiPagamento);
            fieldEstremiPagamento.click();

            if (null != customer.getSezioni()) {

            } else {

            }

        }
    }

    public void apriAnagraficaPerFiltri(Customer cliente) {
        String currentUrl = driver.getCurrentUrl();
        String contiene = "e";
        String iniziaPer = "c";
        List<String> parametriRicerca = new ArrayList<String>();
        if (!cliente.getIdCRM().equals("")) {
            driver.get(currentUrl + "?module=Contacts&view=ModificaAnagrafica&record=" + cliente.getIdCRM());
        } else {
            if (!cliente.getNazioneNascita().equals("")) {
                parametriRicerca.add("[\"cf_1815\",\"" + contiene + "\",\"" + cliente.getNazioneNascita() + "\"]");
            }
            if (null != cliente.getUtilizzatore()) {
                if (cliente.getUtilizzatore().equals(true)) {
                    parametriRicerca.add("[\"cf_1705\",\"" + contiene + "\",\"1\"]");
                } else {
                    parametriRicerca.add("[\"cf_1705\",\"" + contiene + "\",\"0\"]");
                }
            }
            if (!cliente.getTiposocieta().equals("")) {
                parametriRicerca.add("[\"cf_1825\",\"" + contiene + "\",\"" + cliente.getTiposocieta() + "\"]");
            }
            if (!cliente.getCognome().equals("")) {
                parametriRicerca.add("[\"lastname\",\"" + contiene + "\",\"" + cliente.getCognome() + "\"]");
            }
            if (null != cliente.getAventeDiritto()) {
                if (cliente.getAventeDiritto().equals(true)) {
                    parametriRicerca.add("[\"cf_1703\",\"" + contiene + "\",\"1\"]");
                } else {
                    parametriRicerca.add("[\"cf_1703\",\"" + contiene + "\",\"0\"]");
                }

            }
            if (parametriRicerca.size() > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("[[");
                for (int i = 0; i < parametriRicerca.size(); i++) {
                    sb.append(parametriRicerca.get(i));
                    if (!(i == parametriRicerca.size() - 1)) {
                        sb.append(",");
                    }
                }

                sb.append("]]");
                String decodedUrl = sb.toString();
                String encodedUrl = java.net.URLEncoder.encode(decodedUrl);
                driver.get(currentUrl + "?module=Contacts&parent=&page=1&view=List&viewname=7&orderby=&sortorder=&search_params=" + encodedUrl);
                wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("tr.listViewEntries")));
                List<WebElement> risultatiTrovati = driver.findElements(By.cssSelector("tr.listViewEntries"));
                if (risultatiTrovati.size() > 0) {
                    WebElement primoRisultato = risultatiTrovati.get(0);
                    primoRisultato.findElement(By.cssSelector("i.icon-pencil")).click();
                }
            }
        }
    }


}
