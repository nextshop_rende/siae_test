package CRM;
import Tools.Customer;

public class ModificaAnagraficaCliente {
    public static void main(String[] args){
        Customer cliente = new Customer();
        //cliente.setIdCRM("12121365"); //PRODUZIONE
        //cliente.setIdCRM("14603821"); //TEST
        cliente.setUtilizzatore(true);
        //cliente.setCognome("SMILES MAKERS SRLS");
        //cliente.setTiposocieta("SDC");
        cliente.setAventeDiritto(true);
        ApplicazioneCrm applicazione = new ApplicazioneCrm();
        applicazione.initLog("Modifica anagrafica avente diritto sdc");
        applicazione.connect("locale");
        applicazione.apriAnagraficaPerFiltri(cliente);
        applicazione.endLog();
    }
}
