package CRM;
import Tools.Customer;
public class IscrizioneOfflinePersonaGiuridicaCodiceFiscaleEsistente {
    public static void main(String[] args){
        Customer newIscritto = new Customer();
        newIscritto.setNaturaGiuridica("S.P.A.");
        newIscritto.setTipoIscrizione("ASSOCIATO");
        newIscritto.setCategoria("EDITORE");
        //newIscritto.setCodiceFiscaleRappresentante("DTRMRC80A01H501S");
        ApplicazioneCrm iscrizione = new ApplicazioneCrm();
        iscrizione.initLog("Iscrizione Offline Persona Giuridica Editore");
        iscrizione.connect("BAMBOO");
        iscrizione.apriElencoIscrizioniOffline();
        iscrizione.apriNuovaIscrizioneOffline();
        iscrizione.compilaFormIscrizioneOffline(newIscritto);
        iscrizione.endLog();
    }
}
