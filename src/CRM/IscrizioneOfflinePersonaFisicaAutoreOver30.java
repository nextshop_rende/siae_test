package CRM;

import Tools.Customer;
import Tools.Diritto;
import Tools.Sezione;
import Tools.Territorio;

import java.util.ArrayList;
import java.util.List;

public class IscrizioneOfflinePersonaFisicaAutoreOver30 {
    public static void main(String[] args){
        Customer newIscritto = new Customer();
        newIscritto.setNaturaGiuridica("PERSONA FISICA");
        newIscritto.setTipoIscrizione("ASSOCIATO");
        newIscritto.setCategoria("AUTORE");
        newIscritto.setCodiceFiscale("RZUMRC63D15F839C");
        List<Territorio> territori = new ArrayList<>();
        Territorio territorio1 = new Territorio();
        territorio1.setIdTerritorio(2136);
        territorio1.setNomeTerritorio("WORLD");
        territorio1.setIncluso(true);
        Territorio territorio2 = new Territorio();
        territorio2.setIdTerritorio(2120);
        territorio2.setNomeTerritorio("EUROPE");
        territorio2.setIncluso(false);
        territori.add(territorio1);
        territori.add(territorio2);
        Diritto diritto1 = new Diritto();
        List<Diritto> diritti = new ArrayList<>();
        diritto1.setIdDiritto(1);
        diritto1.setNomeDiritto("DIRITTO DI PUBBLICA ESECUZIONE");
        diritto1.setTerritori(territori);
        diritti.add(diritto1);
        Sezione sezione1 = new Sezione();
        sezione1.setIdSezione(2);
        sezione1.setNomeSezione("MUSICA");
        sezione1.setDiritti(diritti);
        List<Sezione> sezioni = new ArrayList<>();
        sezioni.add(sezione1);
        newIscritto.setSezioni(sezioni);
        ApplicazioneCrm iscrizione = new ApplicazioneCrm();
        iscrizione.initLog("Iscrizione Offline Persona Fisica Autore Over 30");
        iscrizione.connect("BAMBOO");
        iscrizione.apriElencoIscrizioniOffline();
        iscrizione.apriNuovaIscrizioneOffline();
        iscrizione.compilaFormIscrizioneOffline(newIscritto);
        iscrizione.endLog();
    }
}
