package CRM;

import Tools.Customer;

public class IscrizioneOfflinePersonaGiuridicaEditore {
    public static void main(String[] args){
        Customer newIscritto = new Customer();
        newIscritto.setNaturaGiuridica("S.N.C.");
        newIscritto.setTipoIscrizione("ASSOCIATO");
        newIscritto.setCategoria("EDITORE");
        ApplicazioneCrm iscrizione = new ApplicazioneCrm();
        iscrizione.connect("BAMBOO");
        iscrizione.apriElencoIscrizioniOffline();
        iscrizione.apriNuovaIscrizioneOffline();
        iscrizione.compilaFormIscrizioneOffline(newIscritto);
    }
}
