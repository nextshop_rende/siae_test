package ECM;

public class UploadDownloadFile {

    public static void main(String[] args) {

        ApplicationFile uploadDownloadFile = new ApplicationFile();
        uploadDownloadFile.initLog("Upload & Download automatico di un file");
        uploadDownloadFile.connect("BAMBOO");
        uploadDownloadFile.uploadDownloadDiUnFile();

    }
}
