package ECM;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Date;

public class ApplicationFile extends Connect {

    public static void main(String[] args) {
    }

    public void initLog(String logName) {
        super.initLog(logName);
    }

    public void endLog() {
        super.endLog();
    }

    public void connect(String ambiente) {
        logger.info("Avvio la connessione all'ambiente " + ambiente);
        String url = "";
        try {
            this.getBrowser();
            logger.info("Connessione avvenuta con successo.");
        } catch (Exception e) {
            logger.severe("Si è verificata la seguente eccezione nel corso della connessione " + e.getMessage());
        }
        driver.manage().window().maximize();
        logger.info("Inizio le operazioni di login");
        switch (ambiente) {
            case "locale":
                url = "http://localhost:8080/share/page/";
                driver.get(url);
                driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-username")).sendKeys("admin");
                driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-password")).sendKeys("Alkemy00a");
                driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-submit-button")).click();
                logger.info("Login avvenuto con successo");
                break;
            case "BAMBOO":
                url = "http://srvtst-l108v.net.siae/share/page?pt=login";
                driver.get(url);
                driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-username")).sendKeys("admin");
                driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-password")).sendKeys("&p9H!YrDRlG6YBG");
                driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-submit-button")).click();
                logger.info("Login avvenuto con successo");
                break;
            case "produzione":
                url = "http://";
                driver.get(url);
                driver.findElement(By.id(" ")).sendKeys(" ");
                driver.findElement(By.id(" ")).sendKeys(" ");
                driver.findElement(By.id(" ")).click();
                driver.findElement(By.id(" ")).click();
                logger.info("Login avvenuto con successo");
                break;
            default:
                if (driver instanceof JavascriptExecutor) {
                    ((JavascriptExecutor) driver)
                            .executeScript("alert('Non è possibile proseguire perché l\\'ambiente impostato non esiste');");
                }
                logger.severe("Non è possibile proseguire perché l'ambiente impostato " + ambiente + "non esiste");
                return;
        }
        this.finestraPrincipale = driver.getWindowHandle();
    }

    public void uploadDownloadDiUnFile(){

        String idCRMLM =  "12345";
        String codiceLicenzaLM =  "678910";
        String argomentoLM =  "Licenze";
        String specificaLM =  "Appunti";
        String tipologiaLicenzaLM =  "Streaming";
        String dataAttivitaLM =  "10/12/2017";
        String serviziAttivitaLM =  "prova, BAMBOO, verifica";
        String tipologiaSitoLM =  "Atto Integrativo";

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n\nINIZIO TEST: "+dataInizio+"\n");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("HEADER_SITES_MENU_text")));
        driver.findElement(By.id("HEADER_SITES_MENU_text")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("HEADER_SITES_MENU_RECENT_goal_text")));
        driver.findElement(By.id("HEADER_SITES_MENU_RECENT_goal_text")).click();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Raccolta documenti")));
        driver.findElement(By.linkText("Raccolta documenti")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Mutimedialità")));
        driver.findElement(By.linkText("Mutimedialità")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Licenze")));
        driver.findElement(By.linkText("Licenze")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("2015")));
        driver.findElement(By.linkText("2015")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("123456789632145")));
        driver.findElement(By.linkText("123456789632145")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Atti Integrativi")));
        driver.findElement(By.linkText("Atti Integrativi")).click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("template_x002e_documentlist_v2_x002e_documentlibrary_x0023_default-fileUpload-button-button")));
        driver.findElement(By.id("template_x002e_documentlist_v2_x002e_documentlibrary_x0023_default-fileUpload-button-button")).click();

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("files[]")));
        driver.findElement(By.name("files[]")).click();

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("files[]")));
        driver.findElement(By.name("files[]")).sendKeys("/home/ciullo/Scrivania/documentoDiTest.txt");

        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_ESCAPE);
            robot.keyRelease(KeyEvent.VK_ESCAPE);
        }
        catch (AWTException e)
        {
            e.printStackTrace();
        }

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("prop_siae_idCRMLM")));
        driver.findElement(By.name("prop_siae_idCRMLM")).sendKeys(idCRMLM);

        try {
            Thread.sleep(1000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("prop_siae_codiceLicenzaLM")));
        driver.findElement(By.name("prop_siae_codiceLicenzaLM")).sendKeys(codiceLicenzaLM);


        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("prop_siae_specificaLM")));
        driver.findElement(By.name("prop_siae_specificaLM")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form_prop_siae_specificaLM > option:nth-child(2)")));
        driver.findElement(By.cssSelector("#template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form_prop_siae_specificaLM > option:nth-child(2)")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("prop_siae_tipologiaLicenzaLM")));
        driver.findElement(By.name("prop_siae_tipologiaLicenzaLM")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form_prop_siae_tipologiaLicenzaLM > option:nth-child(2)")));
        driver.findElement(By.cssSelector("#template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form_prop_siae_tipologiaLicenzaLM > option:nth-child(2)")).click();


        try {
            Thread.sleep(1000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form_prop_siae_dataInizioAttivitaLM-cntrl-date")));
        driver.findElement(By.id("template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form_prop_siae_dataInizioAttivitaLM-cntrl-date")).sendKeys(dataAttivitaLM);


        try {
            Thread.sleep(1000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("prop_siae_serviziAttiviLM")));
        driver.findElement(By.name("prop_siae_serviziAttiviLM")).sendKeys(serviziAttivitaLM);

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("prop_siae_tipologiaSitoLM")));
        driver.findElement(By.name("prop_siae_tipologiaSitoLM")).click();

        try {
            Thread.sleep(2000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form_prop_siae_tipologiaSitoLM > option:nth-child(2)")));
        driver.findElement(By.cssSelector("#template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form_prop_siae_tipologiaSitoLM > option:nth-child(2)")).click();


        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form-form-submit-button")));
        driver.findElement(By.id("template_x002e_dnd-upload_x002e_documentlibrary_x0023_uploader-plus-metadata-form-form-submit-button")).click();

        try {
            Thread.sleep(6000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("documentoDiTest.txt")));

        if(driver.findElement(By.linkText("documentoDiTest.txt")).isEnabled()){

            System.out.println("L'upload del file È AVVENUTO CON SUCCESSO");

            driver.findElement(By.linkText("documentoDiTest.txt")).click();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("Download")));
            driver.findElement(By.linkText("Download")).click();

            String idCRM =  driver.findElement(By.xpath("//div[1]/fieldset/div[1]/div/span[2]")).getText();
            String codiceLicenza =  driver.findElement(By.xpath("//div[1]/fieldset/div[2]/div/span[2]")).getText();
            String argomento =  driver.findElement(By.xpath("//div[1]/fieldset/div[4]/div/span[2]")).getText();
            String specifica =  driver.findElement(By.xpath("//div[1]/fieldset/div[5]/div/span[2]")).getText();
            String tipologiaLicenza =  driver.findElement(By.xpath("//div[1]/fieldset/div[6]/div/span[2]")).getText();
            String dataAttivita =  driver.findElement(By.xpath("//div[1]/fieldset/div[8]/div/span[2]")).getText();
            String serviziAttivita =  driver.findElement(By.xpath("//div[1]/fieldset/div[9]/div/span[2]")).getText();
            String tipologiaSito =  driver.findElement(By.xpath("//div[1]/fieldset/div[12]/div/span[2]")).getText();


            if((idCRM.equals(idCRMLM))
                   &&((codiceLicenza.equals(codiceLicenzaLM)))
                   &&((argomento.equals(argomentoLM)))
                   &&((specifica.equals(specificaLM)))
                   &&((tipologiaLicenza.equals(tipologiaLicenzaLM)))
                   &&((dataAttivita.equals("Dom 10 Dic 2017")))
                   &&((serviziAttivita.equals(serviziAttivitaLM)))
                   &&((tipologiaSito.equals(tipologiaSitoLM)))
              ){

               System.out.println("\nI metadati sono corretti");

            } else {

               System.out.println("\nI metadati sono ERRATI");
            }

        }else{

            System.out.println("L'upload del file NON È AVVENUTO");
        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\nFINE TEST: "+dataFine);

    }

    public void searchFile() {

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n\nINIZIO TEST: "+dataInizio+"\n");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#HEADER_SEARCH_BOX_DROPDOWN_MENU > img")));
        driver.findElement(By.cssSelector("#HEADER_SEARCH_BOX_DROPDOWN_MENU > img")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#HEADER_SEARCH_BOX_ADVANCED_SEARCH_text > a")));
        driver.findElement(By.cssSelector("#HEADER_SEARCH_BOX_ADVANCED_SEARCH_text > a")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("page_x002e_search_x002e_advsearch_x0023_default-selected-form-button")));
        driver.findElement(By.id("page_x002e_search_x002e_advsearch_x0023_default-selected-form-button")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#yui-gen15 > span.form-type-name.yuimenuitemlabel")));
        driver.findElement(By.cssSelector("#yui-gen15 > span.form-type-name.yuimenuitemlabel")).click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("prop_siae_codiceLicenzaLM")));
        driver.findElement(By.name("prop_siae_codiceLicenzaLM")).sendKeys("678910");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#page_x002e_search_x002e_advsearch_x0023_default-search-button-1-button")));
        driver.findElement(By.cssSelector("#page_x002e_search_x002e_advsearch_x0023_default-search-button-1-button")).click();

        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//span/a/span[2]")));
        String nomeFile =  driver.findElement(By.xpath("//span/a/span[2]")).getText();

        if(nomeFile.equals("documentoDiTest.txt")){

            System.out.println("Il risultato della ricerca è corretto");

        } else {

            System.out.println("Il risultato della ricerca è ERRATO");
        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\nFINE TEST: "+dataFine);

    }

    public void deleteFile(){

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n\nINIZIO TEST: "+dataInizio+"\n");


        driver.get("http://srvtst-l108v.net.siae/share/page/site/goal/documentlibrary?file=documentoDiTest.txt#filter=path%7C%2FMutimedialit%C3%A0%2FLicenze%2F2015%2F123456789632145%2FAtti%20Integrativi");

        if(driver.findElement(By.linkText("documentoDiTest.txt")).isEnabled()){

            driver.findElement(By.linkText("documentoDiTest.txt")).click();
            try {
                Thread.sleep(4000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#onActionDelete > a > span")));
            driver.findElement(By.cssSelector("#onActionDelete > a > span")).click();

            try {
                Thread.sleep(4000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#yui-gen36-button")));
            driver.findElement(By.cssSelector("#yui-gen36-button")).click();

        }

        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\nFINE TEST: "+dataFine);

    }


}
