package ECM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Connect {
    protected WebDriver driver;
    protected String finestraPrincipale = null;


    protected WebDriverWait wait;
    protected WebDriverWait shortwait;
    protected Actions actions;


    protected static final String PATH_TO_CROME = "target/classes/chromedriver/chromedriver"; //proprio path


    protected Logger logger;
    protected FileHandler fh;

    public static void main(String[] args){

    }
    public void initLog(String logName){
        try {
            logger = Logger.getLogger(logName);
            fh = new FileHandler(logName + ".txt");
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.addHandler(fh);
        } catch (Exception e){
            logger.info("Non è stato possibile creare il file di log");
        }
    }
    public void endLog(){
        fh.close();
    }

    protected void getBrowser(){
        System.setProperty("webdriver.chrome.driver", PATH_TO_CROME);
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10000);
        shortwait = new WebDriverWait(driver, 5);
        actions = new Actions(driver);
    }

    protected void selectFromChosen(String name, String value) {
        String id = driver.findElement(By.name(name)).getAttribute("id");
        WebElement chosen = driver.findElement(By.id(id + "_chzn"));
        List<WebElement> results = chosen.findElements(By.cssSelector(".chzn-results li"));
        boolean found = false;
        WebElement elementToClick = null;
        for (WebElement we : results) {
            if (we.getAttribute("innerHTML").equals(value)) {
                found = true;
                elementToClick = we;
                break;
            }
        }
        if (found) {
            chosen.findElement(By.cssSelector("a")).click();
            elementToClick.click();
        }
    }
}
